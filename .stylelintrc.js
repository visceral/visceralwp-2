module.exports = {
    extends: 'stylelint-config-standard',
    plugins: ['stylelint-prettier'],
    rules: {
        'prettier/prettier': true,
        indentation: null,
        'at-rule-empty-line-before': [
            'always',
            {
                except: ['blockless-after-same-name-blockless', 'inside-block'],
                ignore: ['after-comment'],
                severity: 'warning',
            },
        ],
        'at-rule-no-unknown': [
            null,
            {
                ignoreAtRules: [
                    'extend',
                    'at-root',
                    'debug',
                    'warn',
                    'error',
                    'if',
                    'include',
                    'else',
                    'for',
                    'each',
                    'while',
                    'mixin',
                    'include',
                    'content',
                    'return',
                    'function',
                    'tailwind',
                    'apply',
                    'responsive',
                    'variants',
                    'screen',
                ],
            },
        ],
        // 'at-rule-no-unknown': [
        //     true,
        //     {
        //         ignoreAtRules: ['indentation'],
        //     },
        // ],
    },
    // rules: {
    //     'no-empty-source': null,
    //     'string-quotes': 'single',
    //     'at-rule-no-unknown': [
    //         true,
    //         {
    //             ignoreAtRules: [
    //                 'extend',
    //                 'at-root',
    //                 'debug',
    //                 'warn',
    //                 'error',
    //                 'if',
    //                 'else',
    //                 'for',
    //                 'each',
    //                 'while',
    //                 'mixin',
    //                 'include',
    //                 'content',
    //                 'return',
    //                 'function',
    //                 'tailwind',
    //                 'apply',
    //                 'responsive',
    //                 'variants',
    //                 'screen',
    //             ],
    //         },
    //     ],
    // },
};
