# [Sage](https://roots.io/sage/)

[![Packagist](https://img.shields.io/packagist/vpre/roots/sage.svg?style=flat-square)](https://packagist.org/packages/roots/sage)
[![devDependency Status](https://img.shields.io/david/dev/roots/sage.svg?style=flat-square)](https://david-dm.org/roots/sage#info=devDependencies)
[![Build Status](https://img.shields.io/travis/roots/sage.svg?style=flat-square)](https://travis-ci.org/roots/sage)

Sage is a WordPress starter theme with a modern development workflow.

## Features

-   Sass for stylesheets
-   Modern JavaScript
-   [Webpack](https://webpack.github.io/) for compiling assets, optimizing images, and concatenating and minifying files
-   [Browsersync](http://www.browsersync.io/) for synchronized browser testing
-   [Blade](https://laravel.com/docs/5.6/blade) as a templating engine
-   [Controller](https://github.com/soberwp/controller) for passing data to Blade templates
-   CSS framework (optional): [Bootstrap 4](https://getbootstrap.com/), [Bulma](https://bulma.io/), [Foundation](https://foundation.zurb.com/), [Tachyons](http://tachyons.io/), [Tailwind](https://tailwindcss.com/)

See a working example at [roots-example-project.com](https://roots-example-project.com/).

## Requirements

Make sure all dependencies have been installed before moving on:

-   [WordPress](https://wordpress.org/) >= 4.7
-   [PHP](https://secure.php.net/manual/en/install.php) >= 7.1.3 (with [`php-mbstring`](https://secure.php.net/manual/en/book.mbstring.php) enabled)
-   [Composer](https://getcomposer.org/download/)
-   [Node.js](http://nodejs.org/) >= 8.0.0
-   [Yarn](https://yarnpkg.com/en/docs/install)

## Theme installation

Install Sage using Composer from your WordPress themes directory (replace `your-theme-name` below with the name of your theme):

```shell
# @ app/themes/ or wp-content/themes/
$ composer create-project roots/sage your-theme-name
```

To install the latest development version of Sage, add `dev-master` to the end of the command:

```shell
$ composer create-project roots/sage your-theme-name dev-master
```

During theme installation you will have options to update `style.css` theme headers, select a CSS framework, and configure Browsersync.

## Theme structure

```shell
themes/your-theme-name/   # → Root of your Sage based theme
├── app/                  # → Theme PHP
│   ├── Controllers/      # → Controller files
│   ├── admin.php         # → Theme customizer setup
│   ├── filters.php       # → Theme filters
│   ├── helpers.php       # → Helper functions
│   └── setup.php         # → Theme setup
├── composer.json         # → Autoloading for `app/` files
├── composer.lock         # → Composer lock file (never edit)
├── dist/                 # → Built theme assets (never edit)
├── node_modules/         # → Node.js packages (never edit)
├── package.json          # → Node.js dependencies and scripts
├── resources/            # → Theme assets and templates
│   ├── assets/           # → Front-end assets
│   │   ├── config.json   # → Settings for compiled assets
│   │   ├── build/        # → Webpack and ESLint config
│   │   ├── fonts/        # → Theme fonts
│   │   ├── images/       # → Theme images
│   │   ├── scripts/      # → Theme JS
│   │   └── styles/       # → Theme stylesheets
│   ├── functions.php     # → Composer autoloader, theme includes
│   ├── index.php         # → Never manually edit
│   ├── screenshot.png    # → Theme screenshot for WP admin
│   ├── style.css         # → Theme meta information
│   └── views/            # → Theme templates
│       ├── layouts/      # → Base templates
│       └── partials/     # → Partial templates
└── vendor/               # → Composer packages (never edit)
```

## Theme setup

Edit `app/setup.php` to enable or disable theme features, setup navigation menus, post thumbnail sizes, and sidebars.

## Theme development

-   Run `yarn` from the theme directory to install dependencies
-   Update `resources/assets/config.json` settings:
    -   `devUrl` should reflect your local development hostname
    -   `publicPath` should reflect your WordPress folder structure (`/wp-content/themes/sage` for non-[Bedrock](https://roots.io/bedrock/) installs)

### Build commands

-   `yarn start` — Compile assets when file changes are made, start Browsersync session
-   `yarn build` — Compile and optimize the files in your assets directory
-   `yarn build:production` — Compile assets for production

## Documentation

-   [Sage documentation](https://roots.io/sage/docs/)
-   [Controller documentation](https://github.com/soberwp/controller#usage)

## Contributing

Contributions are welcome from everyone. We have [contributing guidelines](https://github.com/roots/guidelines/blob/master/CONTRIBUTING.md) to help you get started.

## Changelog

### 1.0

This is the start of building out the Starter Theme into a robust internal product

-   Adds helper classes for adding overlays, background colors, text colors, text alignment, hiding and margin and padding spacing. Some of these also have options at different breakpoints
-   Adds base styles for all global elements, cards, FAQs, list items, and more
-   Updates visceral_query shortcode to handle more parameters and allow for different pagination options (including AJAX load more) and a carousel option
-   Moves shortcodes to own shortcodes.php file
-   Adds visceral_query_filter shortcode so someone can add a query filter to the page without needing to create a custom page template.
-   Adds default visceral-slider class for creating carousels
-   Adds default single post, list item, list item card, single person, list item person, list item job and single resource templates with all necessary custom fields
-   Adds 404 page that can be customized via ACF Options page
-   Updates reveal animations to use GSAP scrollTrigger instead of Waypoints
-   Builds out styleguide page to show colors, elements, animations, and page builder elements
-   Adds visc_get_the_term_list function to more easily get a list of terms without links
-   Adds authorship function to output post authors, based on ACF relationship or text field
-   Adds visc_form_filter_input function to return markup of input to be used as filter for a custom query
-   Adds PostACF function to return all ACF fields data for a post
-   Adds Custom Nav Walker for all menus (BEM)

### 1.1

-   Updates grid system to be more like Bootstrap with default sizes
-   Adds utility classes that work better for responsive
-   Moves related posts functionality out of visceral_query and into its own shortcode
-   cleans up helper functions and adds "visc\_" prefix to all of them
-   Adds visc_get_distinct_meta_values and visc_get_hierarchical_post_terms functions
