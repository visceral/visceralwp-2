<?php

namespace App;

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    /** Add page slug if it doesn't exist */
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add class if sidebar is active */
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});

/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
});

/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment', 'embed'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__ . '\\filter_templates');
});

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    collect(['get_header', 'wp_head'])->each(function ($tag) {
        ob_start();
        do_action($tag);
        $output = ob_get_clean();
        remove_all_actions($tag);
        add_action($tag, function () use ($output) {
            echo $output;
        });
    });
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    if ($template) {
        echo template($template, $data);
        return get_stylesheet_directory() . '/index.php';
    }
    return $template;
}, PHP_INT_MAX);

/**
 * Render comments.blade.php
 */
add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
        [get_stylesheet_directory(), get_template_directory()],
        '',
        $comments_template
    );

    $data = collect(get_body_class())->reduce(function ($data, $class) use ($comments_template) {
        return apply_filters("sage/template/{$class}/data", $data, $comments_template);
    }, []);

    $theme_template = locate_template(["views/{$comments_template}", $comments_template]);

    if ($theme_template) {
        echo template($theme_template, $data);
        return get_stylesheet_directory() . '/index.php';
    }

    return $comments_template;
}, 100);

/**
 * Allow SVG Uploads
 */
function theme_allow_svg_uploads($mimes)
{
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', __NAMESPACE__ . '\\theme_allow_svg_uploads');
// End Allow SVG Uploads

/**
 * Custom Font Formats
 */
function theme_tiny_mce_formats_button($buttons)
{
    array_unshift($buttons, 'styleselect');
    return $buttons;
}
add_filter('mce_buttons_2', __NAMESPACE__ . '\\theme_tiny_mce_formats_button');

// Callback function to filter the MCE settings
function theme_tiny_mce_custom_font_formats($init_array)
{
    // Define the style_formats array
    $style_formats = array(
        // Each array child is a format with it's own settings
        array(
            'title' => 'Split Divider',
            'selector' => '*',
            'classes' => 'split-divider'
        ),
        array(
            'title' => 'New Section',
            'selector' => 'h1, h2, h3, h4, h5, h6',
            'classes' => 'buffer'
        ),
        array(
            'title' => 'Eyebrow',
            'selector' => 'h1, h2, h3, h4, h5, h6',
            'classes' => 'eyebrow'
        ),
        array(
            'title' => 'Intro',
            'selector' => 'h1, h2, h3, h4, h5, h6, p',
            'classes' => 'intro'
        ),
        array(
            'title' => 'Small Paragraph',
            'selector' => 'p',
            'classes' => 'small'
        ),
        array(
            'title' => 'Text Highlight',
            'selector' => '*',
            'classes' => 'text-primary-highlight'
        ),
        array(
            'title' => 'Button',
            'selector' => 'a',
            'classes' => 'btn button'
        ),
        array(
            'title' => 'Button Chevron',
            'selector' => 'a',
            'classes' => 'btn button btn--chevron'
        ),
        array(
            'title' => 'Button Download',
            'selector' => 'a',
            'classes' => 'btn button btn--download'
        ),
        array(
            'title' => 'Button External',
            'selector' => 'a',
            'classes' => 'btn button btn--external'
        ),
        array(
            'title' => 'Modal (Video)',
            'selector' => 'a',
            'classes' => 'modal-link-video'
        ),
        array(
            'title' => 'Number Ticker',
            'inline' => 'span',
            'classes' => 'visceral-ticker'
        ),
        array(
            'title'    => 'Cite (blockquote)',
            'inline'   => 'cite',
            'selector' => 'blockquote',
        ),
    );
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode($style_formats);
    return $init_array;
}
add_filter('tiny_mce_before_init', __NAMESPACE__ . '\\theme_tiny_mce_custom_font_formats'); // Attach callback to 'tiny_mce_before_init'
// End Custom Font Formats

/**
 * Query for all post statuses so attachments are returned
 */
function visceral_modify_link_query_args($query)
{
    // Attachment post types have status of inherit. This allows them to be included.
    $query['post_status'] = array('publish', 'inherit');
    return $query;
}
add_filter('wp_link_query_args', __NAMESPACE__ . '\\visceral_modify_link_query_args');

/**
 * Link to media file URL instead of attachment page
 */
function visceral_modify_link_query_results($results, $query)
{
    foreach ($results as $result) {
        if ('Media' === $result['info']) {
            $result['permalink'] = wp_get_attachment_url($result['ID']);
        }
    }
    return $results;
}

add_filter('wp_link_query', __NAMESPACE__ . '\\visceral_modify_link_query_results', 10, 2);

/**
 * Filter Search by Post Type
 */
add_filter('searchwp\swp_query\args', function ($args) {
    if (isset($_GET['filter'])) {
        $args['post_type'] = array(sanitize_text_field($_GET['filter']));
    }
    return $args;
});

/**
 * LOWER YOAST META BOX
 */
function yoast_dont_boast($html)
{
    return 'low';
}
add_filter('wpseo_metabox_prio', __NAMESPACE__ . '\\yoast_dont_boast');
// END LOWER YOAST META BOX


// Increase time for public preview
function visceral_nonce_life()
{
    return 60 * 60 * 24 * 365; // 365 days
}

add_filter('ppp_nonce_life', __NAMESPACE__ . '\\visceral_nonce_life');

add_filter('gform_confirmation_anchor', function() {
    return true;
});

/*
 * Custom Archive Post Status for People Post Type
 */

// Registers custom post status
function visceral_custom_post_status()
{
    register_post_status('archive', array(
        'label'                     => _x('Archived', 'person'),
        'public'                    => false,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop('Archived <span class="count">(%s)</span>', 'Archived <span class="count">(%s)</span>'),
    ));
}
add_action('init', __NAMESPACE__ . '\\visceral_custom_post_status');

// Adds Archived Status option to Quick Edit in Post List
function visceral_status_into_inline_edit()
{
    global $post_type;
    if ($post_type === 'person') {
        echo "<script>
		jQuery(document).ready( function() {
			jQuery( 'select[name=\"_status\"]' ).append( '<option value=\"archive\">Archived</option>' );
		});
		</script>";
    }
}
add_action('admin_footer-edit.php', __NAMESPACE__ . '\\visceral_status_into_inline_edit');

// Display Archived post status next to title 
function visceral_display_status_label($statuses)
{
    global $post; // we need it to check current post status
    if (get_query_var('post_status') != 'archive') { // not for pages with all posts of this status
        if (isset($post) && $post->post_status == 'archive') { // если статус поста - Архив
            return array('Archived'); // returning our status label
        }
    }
    return $statuses; // returning the array with default statuses
}

add_filter('display_post_states', __NAMESPACE__ . '\\visceral_display_status_label');

// Allows Archived People to be used in relationship fields
function visceral_relationship_options_filter($options, $field, $the_post)
{

    $options['post_status'] = array('publish', 'schedule', 'archive');

    return $options;
}

add_filter('acf/fields/relationship/query/name=authors', __NAMESPACE__ . '\\visceral_relationship_options_filter', 10, 3);

// Adds ability to archive People post from editor screen
function visceral_custom_status_edit_page()
{
    global $post;
    global $post_type;
    $post_status = $post->post_status;
    if ($post_type === 'person') {
        echo "<script>
		jQuery(document).ready( function() {
			jQuery( 'select#post_status' ).append( '<option value=\"archive\">Archived</option>' );
		});
		</script>";
    }
    if ($post_status === 'archive') {
        echo "<script>
			jQuery(document).ready( function() {
				jQuery( '#post-status-display' ).append( 'Archived' );
			});
		</script>";
    }
}

add_action('admin_footer-post.php', __NAMESPACE__ . '\\visceral_custom_status_edit_page');

// AJAX Load More Handler
function visc_ajax_handler()
{
    $args = isset($_POST['query']) ? $_POST['query'] : '';
    $page = isset($_POST['paged']) ? sanitize_text_field($_POST['paged']) : '';
    // var_dump($_POST['paged']);
    if ($page) {
        $args['paged'] = $page + 1;
    }
    $args['post_status'] = 'publish';
    $post_type = $args['post_type'];
    $keyword_filter = $post_type . '_search';
    if ($form_filters->{$keyword_filter} !== '' && !is_null($form_filters->{$keyword_filter}) && class_exists('SWP_Query')) {
        $args['s'] =
            $form_filters->{$keyword_filter};
        $args['engine'] = $post_type . '_engine';

        $new_query = new \SWP_Query($args);

        // If no keyword, use regular query 	
    } else {
        $new_query = new \WP_Query($args);
    }
    // var_dump($new_query);
    $markup = '';
    foreach ($new_query->posts as $post_obj) {
        // post ID
        $ID = $post_obj->ID;

        ob_start();
        // This is needed to render blade file from php file
        include \App\template_path(locate_template('views/partials/list-item-' . $post_type . '.blade.php'));
        $markup .= ob_get_contents();
        ob_end_clean();
    }
    echo $markup;
    die();
}

add_action('wp_ajax_visc_load_more', __NAMESPACE__ . '\\visc_ajax_handler'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_visc_load_more', __NAMESPACE__ . '\\visc_ajax_handler'); // wp_ajax_nopriv_{action}


// Change defaut gravity form submit button from input to button
add_filter('gform_submit_button', __NAMESPACE__ . '\\email_signup_submit_button', 10, 2);
function update_gform_submit_button($button, $form)
{
    return '<button class="button gform_button" id="gform_submit_button_{$form["id"]}"><span>Submit</span></button>';
}



/**
 * Function for `wpseo_meta_author` filter-hook.
 * 
 * @param string                 $author_name  The article author's display name. Return empty to disable the tag.
 * @param Indexable_Presentation $presentation The presentation of an indexable.
 *
 * @return string
 */
add_filter( 'wpseo_meta_author', __NAMESPACE__ . '\\override_yoast_meta', 10, 1 );
function override_yoast_meta( $author ) {
    if(is_singular('post')) {
        // Default to WP author
        $author         = get_the_author();
        $authors_output = array();
        $authors        = (function_exists('get_field') && get_field($author_field, $id)) ? get_field($author_field, $id) : '';

        // Relationship field, return post ID
        if (!empty($authors)) {
            foreach ($authors as $author) {
                $authors_output[] = get_the_title($author);
            }
        }

        // Custom Author Repeater field
        if (have_rows('custom_author')) {
            $custom = '';
            while (have_rows('custom_author')) : the_row();
                $name  = get_sub_field('name');
                if($name) {
                    $custom .= $name;
                }
            endwhile;
            $authors_output[] = $custom;
        }

        if (!empty($authors_output)) {
            $author = $authors_output;
        }

        return !empty($authors_output) ? $authors_output : '';
    }

    return $author;
}



/**
 *  Update enhanced Slack data on Posts
 */
add_filter('wpseo_enhanced_slack_data', function($data) {
    global $post;
    if ($post->post_type == 'post')
    {
        $ID = $post->ID;
        // Default to WP author
        $author_id = get_post_field( 'post_author', $ID );
        $author = get_the_author_meta( 'display_name', $author_id );

        if(function_exists('get_field')) {
            if( have_rows('custom_authors', $ID) ) {
            	$count = count(get_field('custom_authors', $ID));
            	$authors = array();

                while ( have_rows('custom_authors') ) : the_row();
            		$authors[] = get_sub_field('custom_author');
            		$author = implode(', ', $authors);
                endwhile;
            }elseif( get_field('custom_author', $ID)) { // custom author
                $author = get_field('custom_author', $ID);
            }
        }

        $data['Written by'] = $author;
        return $data;
    }
    else
    {
        return $data;
    }
});
