<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SingleResource extends Controller
{
    public function types()
    {
        $type = wp_get_post_terms(get_the_id(), 'resource_type', array('fields' => 'names'));
        $type = !empty($type) ? implode(', ', $type) : '';
        return $type;
    }

    public function typeLinks()
    {
        $types = wp_get_post_terms(get_the_id(), 'resource_type');
        $links = array();

        if ($types) {
            foreach ($types as $type) {
                $links[] = '<a href="' . get_the_permalink(get_page_by_path('resources')) . '?tax_filter_resource_type=' . $type->slug . '">' . $type->name . '</a>';
            }
        }

        return implode('<br>', $links);
    }

    public function topicLinks()
    {
        $topics = wp_get_post_terms(get_the_id(), 'resource_topic');
        $links = array();

        if ($topics) {
            foreach ($topics as $topic) {
                $links[] = '<a href="' . get_the_permalink(get_page_by_path('resources')) . '?tax_filter_resource_topic=' . $topic->slug . '">' . $topic->name . '</a>';
            }
        }

        return implode('<br>', $links);
    }

    public function topics()
    {
        $topic = wp_get_post_terms(get_the_id(), 'resource_topic', array('fields' => 'names'));
        $topic = !empty($topic) ? implode(', ', $topic) : '';
        return $topic;
    }

    public function fields()
    {
        return \App\visc_get_all_fields(get_the_ID());
    }

    public function thumbnail()
    {
        return (function_exists('get_field') && get_field('thumbnail_image')) ? get_field('thumbnail_image') : '';
    }

    public function relatedHeading()
    {
        return 'Related Resources';
    }

    public function relatedContent()
    {
        return \App\visc_related_posts(3, 'resource');
    }
}
