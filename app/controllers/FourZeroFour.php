<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class FourZeroFour extends Controller
{
    protected $template = '404';
    public function description()
    {
        if (function_exists('get_field') && $fourohfour_options = get_field('404_page', 'option')) {
            return $fourohfour_options['description'];
        } else {
            return false;
        }
    }
    public function backgroundImage()
    {
        if (function_exists('get_field') && $fourohfour_options = get_field('404_page', 'option')) {
            return $fourohfour_options['background_image']['sizes']['large'];
        } else {
            return false;
        }
    }
}
