<?php

namespace App\Controllers;

use Sober\Controller\Controller;

// Files need to be uppercase to work
class App extends Controller
{
    public function siteName()
    {
        return get_bloginfo('name');
    }

    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            global $wp_query;
            if (count($wp_query->posts) === 1) {
                return sprintf(__('result for "%s"', 'sage'), get_search_query());
            }
            else {
                return sprintf(__('results for "%s"', 'sage'), get_search_query());
            }
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }

    public static function formFilters()
    {
        $form_filters = new \stdClass();

        foreach ($_REQUEST as $key => $value) {
            if (is_array($value)) {
                $form_filters->$key = array();
                foreach ($value as $sub_value) {
                    $form_filters->$key[] = sanitize_text_field($sub_value);
                }
            } else {
                $form_filters->$key = sanitize_text_field($value);
            }
        }

        return $form_filters;
    }

    public function mastheadImage()
    {
        // Set up variable
        $img_class           = '';
        // $header_class        = '';
        $parent_id           = wp_get_post_parent_id(get_the_ID()) ? wp_get_post_parent_id(get_the_ID()) : '';
        $image_id            = '';
        $full_size_image_url = '';
        $placeholder_image_url = '';

        // Start off with a check for custom header images early and get the ID
        if (function_exists('get_field') && get_field('custom_header') != '') {
            $custom_header = get_field('custom_header');
            $image_id      = $custom_header['id'];
        }

        // Finally, if nothing else, we check for a featured image and get it's ID
        elseif (has_post_thumbnail()) {
            $image_id = get_post_thumbnail_id();
        }

        // Remember the parent ID's we got above? Let's adopt their main image and get the ID
        if ($parent_id != '') {
            if (function_exists('get_field') && get_field('custom_header', $parent_id) != '') {
                $custom_header = get_field('custom_header', $parent_id);
                $image_id = $custom_header['id'];
            } elseif (has_post_thumbnail($parent_id)) {
                $image_id = get_post_thumbnail_id($parent_id);
            }
        }

        // By now, we should have an image ID to use. Let's use it to get some URLs.
        if ($image_id != '') {
            $full_size_image = wp_get_attachment_image_src($image_id, 'full', true);
            $full_size_image_url = $full_size_image[0];

            $placeholder_image = wp_get_attachment_image_src($image_id, 'thumbnail', true);
            $placeholder_image_url = $placeholder_image[0];

            $img_class = 'masthead--img-bg';

            // Add modifier class to header
            $header_class = 'header--alt';
        }

        $image = new \stdClass();

        $image->full_size_image_url   = $full_size_image_url;
        $image->placeholder_image_url = $placeholder_image_url;
        $image->img_class             = $img_class;
        // $image->header_class          = $header_class;

        return $image;
    }

    public function socialLinks()
    {
        // Get values from customizer fields
        // and return as list of links

        $social = array();
        $links  = '';

        $social['facebook'] = get_theme_mod('facebook_url');
        $social['twitter']  = get_theme_mod('twitter_url');
        $social['instagram']  = get_theme_mod('instagram_url');
        $social['vimeo']  = get_theme_mod('vimeo_url');
        $social['youtube']  = get_theme_mod('youtube_url');
        $social['linkedin'] = get_theme_mod('linkedin_url');

        if ($social) {
            $links = '<ul class="social-links list-inline no-print">';
            foreach ($social as $channel => $link) {
                if (!empty($link)) {
                    $links .= '<li><a href="' . $link . '" class="social-links__link social-link--' . $channel . '"><i class="icon-' . $channel . ' icon" aria-hidden="true"></i><span class="screen-reader-text">' . $channel . '</span></a></li>';
                }
            }
            $links .= '</ul>';
        }

        return $links;
    }

    public function headerOptions()
    {
        if (is_search()) {
            return 'site-header--dark-text';
        }
        if (is_singular('person')) {
            return 'site-header--dark-text';
        } elseif (is_single()) {
            if (!has_post_thumbnail()) {
                return 'site-header--dark-text';
            }
            return 'single';
        } elseif (is_page()) {
            if (function_exists('get_field') && (get_field('header_text_color') === 'dark')) {
                return 'site-header--dark-text';
            }
        } else {
            return;
        }
    }
}
