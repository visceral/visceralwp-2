<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class Search extends Controller
{
    public function originalSearch()
    {
        // Save the original query
        $original_search = isset($_GET['s']) ? sanitize_text_field($_GET['s']) : '';

        return $original_search;
    }

    public function total()
    {
        global $wp_query;
        return $wp_query->found_posts;
    }

    public function begin()
    {
        global $wp_query;
        $paged         = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $post_per_page = $wp_query->query_vars['posts_per_page'];
        $offset        = ($paged - 1) * $post_per_page;
        $begin         = $offset + 1;

        return $begin;
    }

    public function end()
    {
        global $wp_query;
        $total         = $wp_query->found_posts;
        $paged         = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $post_per_page = $wp_query->query_vars['posts_per_page'];
        $end           = ($paged * $post_per_page < $total) ? $paged * $post_per_page : $total;

        return $end;
    }

    public function filterItems()
    {
        // Create content for filter dropdown
        $post_types  = get_post_types(array('public' => true));
        $filter_items = '';

        if ($post_types) {
            $filter_items = '<option value="">' . __('Filter', 'visceral') . '</option>';
            foreach ($post_types as $slug) {
                if ($slug != 'attachment' && $slug != 'e-landing-page' && $slug != 'elementor_library') {
                    $label = get_post_type_object($slug)->labels->name;
                    $filter_selected = (isset($_GET['post_type']) &&  sanitize_text_field($_GET['post_type']) == $slug) ? 'selected' : '';
                    $filter_items .= '<option name="content_filter" value="' . $slug . '"' . $filter_selected . '>' . $label . '</option>';
                }
            }
        } else {
            $filter_items .= '<option>' . __('No filters', 'visceral') . '</option>';
        }

        return $filter_items;
    }

    public function pagination()
    {
        global $wp_query;
        $found_posts    = $wp_query->found_posts;
        $posts_per_page = 10;
        $total          = ceil($found_posts / $posts_per_page);
        $big            = 999999999; // need an unlikely integer
        $current_page   = max(1, get_query_var('paged'));

        $args = array(
            'base'      => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
            'format'    => '?paged=%#%',
            'current'   => $current_page,
            'total'     => $total,
            'prev_text' => '<span class="icon icon-chevron-left"></span><span class="screen-reader-text">' . __(' Previous', 'visceral') . '</span>',
            'next_text' => '<span class="screen-reader-text">' . __('Next ', 'visceral') . '</span><span class="icon icon-chevron-right"></span>'
        );

        return paginate_links($args);
    }
}
