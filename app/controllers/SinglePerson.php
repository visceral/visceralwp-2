<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SinglePerson extends Controller
{
    public function image()
    {
        if ($image = get_the_post_thumbnail(get_the_id(), 'medium', array('class' => 'img-crop'))) {
            return $image;
        } else {
            return false;
        }

    }

    public function jobTitle()
    {
        if ($job_title = get_post_meta(get_the_id(), 'job_title', true)) {
            return $job_title;
        } else {
            return false;
        }
    }

    public function organization()
    {
        if ($organization = get_post_meta(get_the_id(), 'organization', true)) {
            return $organization;
        } else {
            return false;
        }
    }

    public function email()
    {
        if ($email = get_post_meta(get_the_id(), 'email', true)) {
            return $email;
        } else {
            return false;
        }
    }

    public function twitter()
    {
        if ($twitter = get_post_meta(get_the_id(), 'twitter', true)) {
            return $twitter;
        } else {
            return false;
        }
    }

    public function linkedin()
    {
        if ($linkedin = get_post_meta(get_the_id(), 'linkedin', true)) {
            return $linkedin;
        } else {
            return false;
        }
    }

    public function website()
    {
        if ($website = get_post_meta(get_the_id(), 'website', true)) {
            return $website;
        } else {
            return false;
        }
    }

    public function phone()
    {
        if ($phone = get_post_meta(get_the_id(), 'phone', true)) {
            return $phone;
        } else {
            return false;
        }
    }

    public function first_name()
    {
        if ($first_name = get_post_meta(get_the_id(), 'first_name', true)) {
            return $first_name;
        } else {
            return false;
        }
    }

    public function relatedHeading()
    {
        if ($first_name = get_post_meta(get_the_id(), 'first_name', true)) {
            return 'Authored by ' . $first_name;
        }
    }

    public function relatedContent()
    {
        $output = array();
        $related_content = get_posts(array(
            'post_type' => 'post',
            'posts_per_page' => -1,
            'orderby' => 'date',
            'order' => 'DESC',
            'meta_query' => array(
                array(
                    'key' => 'authors',
                    'value' => '"' . get_the_id() . '"',
                    'compare' => 'LIKE'
                )
            )
        ));

        if ($related_content) {
            return $related_content;
        }
    }
}
