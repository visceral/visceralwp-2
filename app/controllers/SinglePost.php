<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SinglePost extends Controller
{

    public function categories()
    {
        $categories    = array();
        // First, retrieve categories and add #
        $rawCategories = wp_get_post_categories(get_the_id(), array('fields' => 'all'));

        foreach ($rawCategories as $category) {
            $categories[] = '<span>' . $category->name . '</span>';
        }

        // Return as comma separated list
        $categories = implode(", ", $categories);

        return $categories;
    }

    public function relatedContent()
    {
        return \App\visc_related_posts();
    }
}
