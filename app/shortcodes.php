<?php

namespace App;

/**
 * Visceral Related
 *
 * @return  The markup for a list of posts based on a related content field.
 */
function visceral_related_shortcode($atts)
{
    extract(shortcode_atts(array(
        'container_class' => '', //Wrapper class
        'template'        => 'views/partials/list-item', //which individual post template to use
        'related_field' => 'related_content', //which field to use for related content
        'related_heading' => 'Related', //heading for related content
        'auto_fill' => true, //whether to auto fill posts after related content posts are exhausted
        'post_type'       => 'post', //which post type to use
        'posts_per_page'  => 3, //how many posts to show
        'taxonomy'        => '', //which taxonomy to use for auto fill
        'terms'           => '', //which terms to use for auto fill
        'slider'          => false, //whether to turn list of posts into a slider
        'slides_to_show'  => 3 //how many slides to show in the slider
    ), $atts));

    // Set the string 'false' to boolean
    $auto_fill = $auto_fill === 'false' ? false : true;
    $markup = '';
    // If related posts are chosen, let's grab those
    $related_posts = visc_related_posts($posts_per_page, $post_type, $related_field, $taxonomy, $terms, $auto_fill);

    // If we have related posts, let's display them
    if ($related_posts) {
        $markup .= '<section class="related-content text-center p-t-64 p-b-24 md-p-b-120 posts-list-container ' . ($slider ? 'visceral-slider' : '') . '" data-slides-to-show="' . $slides_to_show . '">';
        $markup .= '<div class="' . $container_class . '">';
        $markup .= '<h2 class="h5 related-content__heading">';
        $markup .= $related_heading . '</h2>';
        $markup .= '<div class="row">';
        // Display related posts
        if ($related_posts) {
            foreach ($related_posts as $related_post) {
                ob_start();
                $ID = $related_post->ID;
                include \App\template_path(locate_template($template . '.blade.php'));
                $markup .= ob_get_contents();
                ob_end_clean();
            }
        }
        $markup .= '</div>';
        $markup .= '</div>';
        $markup .= '</section>';
    }
    return $markup;
}
add_shortcode('visceral_related', __NAMESPACE__ . '\\visceral_related_shortcode');

/**
 * Visceral Query
 *
 * @return  The markup for a list of posts based on query parameters and template files.
 */
// TODO: Add options for ajax loading and infinite scroll
function visceral_query_shortcode($atts)
{
    extract(shortcode_atts(array(
        'container_class' => '', //Wrapper class
        'template'        => 'views/partials/list-item', //which individual post template to use
        'pagination'      => false, //whether to show pagination
        'no_posts'        => __('Sorry, there isn\'t any content that matches that criteria. Please try again.', 'visceral'), //message to display when no posts are found
        'post_type'       => 'post', //which post type to use
        'posts_per_page'  => 6, //how many posts to show
        'offset'          => 0, //how many posts to skip
        'order'           => 'DESC', //order of posts
        'orderby'         => 'post_date', //order by
        'meta_key'        => '', //which meta key to use for ordering
        'post__not_in'    => '', //which posts to exclude
        'taxonomy'        => '', //which taxonomy to use
        'terms'           => '', //which terms to use
        'post_parent'     => 0, // default only parent posts
        'slider'          => false, //whether to turn list of posts into a slider
        'slides_to_show'  => 3 //how many slides to show in the slider
    ), $atts));

    $markup = '';

    // Return either a single post type or an array of post types depending on how many were passed in.
    // Note: We need to do this because the Intuitive Custom Post Type plugin won't work with an array of a single post type.
    $post_type = (strpos($post_type, ',') > 0) ? explode(', ', $post_type) : $post_type;
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $form_keyword = false;
    $args = array(
        'post_type'      => $post_type,
        'posts_per_page' => $posts_per_page,
        'paged'          => $paged,
        'order'          => $order,
        'orderby'        => $orderby,
        'meta_key'       => $meta_key,
        'post__not_in'   => explode(', ', $post__not_in),
    );

    if ($taxonomy && $terms) {
        // Explode into array for multiple taxonomies
        $taxonomies = explode(', ', $taxonomy);

        $args['tax_query'] = array(
            'relation' => 'OR',
        );

        foreach ($taxonomies as $tax) {
            $args['tax_query'][] = array(
                'taxonomy' => $tax,
                'field'     => 'slug',
                'terms'    => explode(', ', $terms),
            );
        }
    }

    $form_filters = Controllers\App::formFilters();
    // If isn't in backend (elementor editor view) or preview
    if (!is_admin() && !property_exists($form_filters, 'preview')) {
        // If there are form filters
        if ($form_filters) {
            foreach ($form_filters as $key => $value) {
                if (!empty($value)) {
                    // Reset taxonomy relation
                    $args['tax_query'] = array(
                        'relation' => 'AND',
                    );
                    // If this is a tax filter and the filter has a value
                    if (strpos($key, 'tax_filter_') === 0 && $value) {
                        $args['tax_query'][] = array(
                            'taxonomy' => substr($key, 11),
                            'field'     => 'slug',
                            'terms'    => $value,
                        );
                    }
                }
            }
        }
    }    
    // If a keyword was entered and SearchWP is installed
    if ($form_keyword && $form_filters->$form_keyword !== '' && class_exists('SWP_Query')) {
        $args['s'] = $form_filters->$form_keyword;
        $args['engine'] = $form_keyword;

        $query = new \SWP_Query($args);
    } else {
        // If no keyword, use regular query 
        $query = new \WP_Query($args);
    }

    if ($query->have_posts()) :
        $markup .= '<div class="posts-list-container ' . ($slider ? 'visceral-slider' : '') . '" data-slides-to-show="' . $slides_to_show . '">';
        $markup .= '<div class="' . $container_class . '">';

        while ($query->have_posts()) :
            $query->the_post();
            ob_start();
            $ID = get_the_id();
            include \App\template_path(locate_template($template . '.blade.php'));
            $markup .= ob_get_contents();
            ob_end_clean();
        endwhile;
        $markup .= '</div>';
        $markup .= '</div>';
        wp_reset_postdata();

        // This is for the ajax loading
        // Security nonce
        $visceral_nonce = wp_create_nonce("visceral-ajax-nonce");
        // Variables to send to JS
        wp_localize_script('sage/main.js', 'visceral_vars', array(
            'ajaxurl' => admin_url('admin-ajax.php'),
            'query' => $query->query,
            'current_page' => $query->query_vars['paged'] ? $query->query_vars['paged'] : 1,
            'max_page' => $query->max_num_pages,
            'security' => $visceral_nonce
        ));

        // Pagination
        $total = $query->max_num_pages;
        // only bother with pagination if this isn't a slider, we have more than 1 page, and pagination is declared
        if (!$slider && $total > 1 && $pagination) :
            if ($pagination === 'default') :
                $big = 999999999; // need an unlikely integer
                $current_page = max(1, get_query_var('paged'));
                $pagination = paginate_links(array(
                    'base'      => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
                    'format'    => '?paged=%#%',
                    'current'   => $current_page,
                    'total'     => $total,
                    'type'      => 'plain',
                    'prev_next' => true,
                    'prev_text' => '<svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg"> <rect width="32" height="32" fill="white"/> <path d="M20.875 22.125L19 24L11 16L19 8L20.875 9.875L14.75 16L20.875 22.125Z" fill="#03061B"/> </svg> <span class="screen-reader-text">' . __('Previous', 'visceral') . '</span>',
                    'next_text' => '<svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg"> <rect width="32" height="32" fill="white"/> <path d="M11 9.875L12.875 8L20.875 16L12.875 24L11 22.125L17.125 16L11 9.875Z" fill="#03061B"/> </svg> <span class="screen-reader-text">' . __('Next', 'visceral') . '</span>'
                ));

                $markup .= '<nav class="pagination text-center">';
                $markup .= $pagination;
                $markup .= '</nav>';
            endif;
            if ($pagination === 'load-more') :
                $markup .= '<button class="btn--load-more">' . __('Load More', 'visceral') . '</button>';
            endif;
        endif;
    else :
        $markup = html_entity_decode($no_posts);
    endif;

    return $markup;
}
add_shortcode('visceral_query', __NAMESPACE__ . '\\visceral_query_shortcode');


/** Create a form to filter a custom query on the same page */
// TODO: Add ability to handle filters based on metadata
add_shortcode('visceral_query_filter', function ($atts) {
    extract(shortcode_atts(array(
        'container_class' => '', // Class for the form container
        'taxonomies' => '', // taxonomies to create form inputs from
        'input_type' => 'select', //type of input to create
        'search' => false, // This needs to be the SearchWP engine slug
        'display' => '', // Type of display. Defaults to block but can also be inline
        'auto_submit' => false, // If true, the form will submit on change
        'clear_filter' => false, // If true, adds a clear button to the form
    ), $atts));

    $markup = '';
    $markup .= '<div class="visc-query-filter ' . $container_class . ' ' . ($display ? ('visc-query-filter--' . $display) : '') . '">';
    if ($taxonomies || $search) {

        $markup .= '<form class="visc-query-filter__form" action="">';
        if ($search) {
            $markup .= '<label for="' . $search . '">';
            $markup .= '<span class="screen-reader-text">' . __('Search by Keyword', 'visceral') . '</span>';
            $markup .= '<input type="text" id="' . $search . '" name="' . $search . '" placeholder="' . __('Search by Keyword', 'visceral') . '" value="' . (isset($_GET[$search]) ? sanitize_text_field($_GET[$search]) : '') . '">';
            $markup .= '</label>';
        }
        if ($taxonomies) {
            // Check to see if there are multiple taxonomies
            if (strpos($taxonomies, ',')) {
                $taxonomies = explode(',', $taxonomies);
            } else {
                $taxonomies = [$taxonomies];
            }
            // Loop through each taxonomy and create an input for each one
            foreach ($taxonomies as $taxonomy) {
                $markup .= visc_form_filter_input($taxonomy, $input_type, false, $auto_submit);
            }
        }
        // Hide submit button if auto-complete is true
        if ($auto_submit) {
            $markup .= '<button type="submit" class="' . ($auto_submit ? 'js-hide' : '') . '">' . __('Submit', 'visceral') . '</button>';
        }
        // Add clear filter link if it is on
        if ($clear_filter) {
            $markup .= '<p class="small"><a href="' . get_permalink() . '" class="visc-query-filter__clear">' . __('Clear Filter', 'visceral') . '</a></p>';
        }
        $markup .= '</form>';
    }
    $markup .= '</div>';

    return $markup;
});
