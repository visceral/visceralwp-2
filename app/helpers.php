<?php

namespace App;

use Roots\Sage\Container;

/**
 * Get the sage container.
 *
 * @param string $abstract
 * @param array  $parameters
 * @param Container $container
 * @return Container|mixed
 */
function sage($abstract = null, $parameters = [], Container $container = null)
{
    $container = $container ?: Container::getInstance();
    if (!$abstract) {
        return $container;
    }
    return $container->bound($abstract)
        ? $container->makeWith($abstract, $parameters)
        : $container->makeWith("sage.{$abstract}", $parameters);
}

/**
 * Get / set the specified configuration value.
 *
 * If an array is passed as the key, we will assume you want to set an array of values.
 *
 * @param array|string $key
 * @param mixed $default
 * @return mixed|\Roots\Sage\Config
 * @copyright Taylor Otwell
 * @link https://github.com/laravel/framework/blob/c0970285/src/Illuminate/Foundation/helpers.php#L254-L265
 */
function config($key = null, $default = null)
{
    if (is_null($key)) {
        return sage('config');
    }
    if (is_array($key)) {
        return sage('config')->set($key);
    }
    return sage('config')->get($key, $default);
}

/**
 * @param string $file
 * @param array $data
 * @return string
 */
function template($file, $data = [])
{
    return sage('blade')->render($file, $data);
}

/**
 * Retrieve path to a compiled blade view
 * @param $file
 * @param array $data
 * @return string
 */
function template_path($file, $data = [])
{
    return sage('blade')->compiledPath($file, $data);
}

/**
 * @param $asset
 * @return string
 */
function asset_path($asset)
{
    return sage('assets')->getUri($asset);
}

/**
 * @param string|string[] $templates Possible template files
 * @return array
 */
function filter_templates($templates)
{
    $paths = apply_filters('sage/filter_templates/paths', [
        'views',
        'resources/views'
    ]);
    $paths_pattern = "#^(" . implode('|', $paths) . ")/#";

    return collect($templates)
        ->map(function ($template) use ($paths_pattern) {
            /** Remove .blade.php/.blade/.php from template names */
            $template = preg_replace('#\.(blade\.?)?(php)?$#', '', ltrim($template));

            /** Remove partial $paths from the beginning of template names */
            if (strpos($template, '/')) {
                $template = preg_replace($paths_pattern, '', $template);
            }

            return $template;
        })
        ->flatMap(function ($template) use ($paths) {
            return collect($paths)
                ->flatMap(function ($path) use ($template) {
                    return [
                        "{$path}/{$template}.blade.php",
                        "{$path}/{$template}.php",
                    ];
                })
                ->concat([
                    "{$template}.blade.php",
                    "{$template}.php",
                ]);
        })
        ->filter()
        ->unique()
        ->all();
}

/**
 * @param string|string[] $templates Relative path to possible template files
 * @return string Location of the template
 */
function locate_template($templates)
{
    return \locate_template(filter_templates($templates));
}

/**
 * Determine whether to show the sidebar
 * @return bool
 */
function display_sidebar()
{
    static $display;
    isset($display) || $display = apply_filters('sage/display_sidebar', false);
    return $display;
}

/**
 * Walker Texas Ranger
 * Inserts some BEM naming sensibility into Wordpress menus
 */

class walker_texas_ranger extends \Walker_Nav_Menu
{

    function __construct($css_class_prefix)
    {

        $this->css_class_prefix = $css_class_prefix;

        // Define menu item names appropriately

        $this->item_css_class_suffixes = array(
            'item'                      => '__item',
            'parent_item'               => '__item--parent',
            'active_item'               => '__item--active',
            'parent_of_active_item'     => '__item--parent--active',
            'ancestor_of_active_item'   => '__item--ancestor--active',
            'sub_menu'                  => '__sub-menu',
            'sub_menu_item'             => '__sub-menu__item',
            'link'                      => '__link',
        );
    }

    // Check for children

    function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output)
    {

        $id_field = $this->db_fields['id'];

        if (is_object($args[0])) {
            $args[0]->has_children = !empty($children_elements[$element->$id_field]);
        }

        return parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
    }

    function start_lvl(&$output, $depth = 1, $args = array())
    {

        $real_depth = $depth + 1;

        $indent = str_repeat("\t", $real_depth);

        $prefix = $this->css_class_prefix;
        $suffix = $this->item_css_class_suffixes;

        $classes = array(
            $prefix . $suffix['sub_menu'],
            $prefix . $suffix['sub_menu'] . '--' . $real_depth
        );

        $class_names = implode(' ', $classes);

        // Add a ul wrapper to sub nav

        $output .= "\n" . $indent . '<ul class="' . $class_names . '">' . "\n";
    }

    // Add main/sub classes to li's and links

    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
    {

        global $wp_query;

        $indent = ($depth > 0 ? str_repeat("    ", $depth) : ''); // code indent

        $prefix = $this->css_class_prefix;
        $suffix = $this->item_css_class_suffixes;

        // Item classes
        $item_classes =  array(
            'item_class'            => $depth == 0 ? $prefix . $suffix['item'] : '',
            'parent_class'          => $args->has_children ? $parent_class = $prefix . $suffix['parent_item'] : '',
            'active_page_class'     => in_array("current-menu-item", $item->classes) ? $prefix . $suffix['active_item'] : '',
            'active_parent_class'   => in_array("current-menu-parent", $item->classes) ? $prefix . $suffix['parent_of_active_item'] : '',
            'active_ancestor_class' => in_array("current-menu-ancestor", $item->classes) ? $prefix . $suffix['ancestor_of_active_item'] : '',
            'depth_class'           => $depth >= 1 ? $prefix . $suffix['sub_menu_item'] . ' ' . $prefix . $suffix['sub_menu'] . '--' . $depth . '__item' : '',
            'item_id_class'         => $prefix . '__item--' . $item->object_id,
            'user_class'            => $item->classes[0] !== '' ? $prefix . '__item--' . $item->classes[0] : ''
        );

        // convert array to string excluding any empty values
        $class_string = implode("  ", array_filter($item_classes));

        // Add the classes to the wrapping <li>
        $output .= $indent . '<li class="' . $class_string . '">';

        // Link classes
        $link_classes = array(
            'item_link'             => $depth == 0 ? $prefix . $suffix['link'] : '',
            'depth_class'           => $depth >= 1 ? $prefix . $suffix['sub_menu'] . $suffix['link'] . '  ' . $prefix . $suffix['sub_menu'] . '--' . $depth . $suffix['link'] : '',
        );

        $link_class_string = implode("  ", array_filter($link_classes));
        $link_class_output = 'class="' . $link_class_string . '"';

        // link attributes
        $attributes  = !empty($item->attr_title) ? ' title="'  . esc_attr($item->attr_title) . '"' : '';
        $attributes .= !empty($item->target)     ? ' target="' . esc_attr($item->target) . '"' : '';
        $attributes .= !empty($item->xfn)        ? ' rel="'    . esc_attr($item->xfn) . '"' : '';
        $attributes .= !empty($item->url)        ? ' href="'   . esc_attr($item->url) . '"' : '';

        // Create link markup
        $item_output = $args->before;
        $item_output .= '<a' . $attributes . ' ' . $link_class_output . '>';
        $item_output .=     $args->link_before;
        $item_output .=     apply_filters('the_title', $item->title, $item->ID);
        $item_output .=     $args->link_after;
        $item_output .=     $args->after;
        $item_output .= '</a>';
        if ($prefix === 'main-menu' && $args->has_children && $depth == 0) {
            $item_output .=     '<button class="' . $prefix . '__sub-menu-toggle" tabindex="0" ariaexpanded="false" aria-label="Toggle Sub-menu"><svg width="8" height="6" viewBox="0 0 8 6" fill="none" xmlns="http://www.w3.org/2000/svg">
<path opacity="1" d="M1 1.5L4 4.5L7 1.5" stroke="#fff" stroke-width="1.5" stroke-linecap="round"/>
</svg></button></span>
';
        }

        // Filter <li>

        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
    }
}

/**
 * visc_bem_menu returns an instance of the walker_texas_ranger class with the following arguments
 * @param  string $location This must be the same as what is set in wp-admin/settings/menus for menu location.
 * @param  string $css_class_prefix This string will prefix all of the menu's classes, BEM syntax friendly
 * @param  arr/string $css_class_modifiers Provide either a string or array of values to apply extra classes to the <ul> but not the <li's>
 * @return [type]
 */

function visc_bem_menu($location = "main_menu", $css_class_prefix = 'main-menu', $css_class_modifiers = null)
{

    // Check to see if any css modifiers were supplied
    if ($css_class_modifiers) {

        if (is_array($css_class_modifiers)) {
            $modifiers = implode(" ", $css_class_modifiers);
        } elseif (is_string($css_class_modifiers)) {
            $modifiers = $css_class_modifiers;
        }
    } else {
        $modifiers = '';
    }

    $args = array(
        'theme_location'    => $location,
        'container'         => false,
        'items_wrap'        => '<ul class="' . $css_class_prefix . ' ' . $modifiers . '">%3$s</ul>',
        'walker'            => new walker_texas_ranger($css_class_prefix, true)
    );

    if (has_nav_menu($location)) {
        return wp_nav_menu($args);
    } else {
        echo "<p>You need to first define a menu in WP-admin<p>";
    }
}

/* GENERAL PURPOSE FUNCTIONS */

/**
 * RELATED POSTS FUNCTION - Creates an array of related post objects
 *
 * @param int   $total_posts                 Total posts to display
 * @param mixed $post_type                   Either a string or array of post types to display
 * @param string $related_posts_field_name   Name of the manually selected related posts ACF field
 * @param string $taxonomy                   Name of the taxonomy to use for fallback posts
 * @param string $term                       Name of the taxonomy term to use for fallback posts
 * @param boolean $auto_fill                  Flag to automatically pull in other posts beyond what is selected in related posts field
 *
 * @return array                             Returns an of related posts
 */
function visc_related_posts($posts_per_page = 3, $post_type = 'post', $related_posts_field_name = 'related_posts', $taxonomy = false, $terms = false, $auto_fill = true)
{
    // Set up the an array to hold them
    $related_posts = array();
    $selected_posts_ids = array();
    // Get post ID's from related field
    if (function_exists('get_field')) {
        $selected_posts = get_field($related_posts_field_name);
    }
    // Add each post object to our array
    // Subtract each post from total posts
    if ($selected_posts) {
        foreach ($selected_posts as $post_object) {
            array_push($related_posts, $post_object);
            array_push($selected_posts_ids, $post_object->ID);
            $posts_per_page--;
        }
    }

    // If we need more posts, let's get some from recent posts

    if ($auto_fill && $posts_per_page) {
        // Don't repeat the posts that we already have
        $exclude_posts = $selected_posts_ids; // Make sure it's an array
        array_push($exclude_posts, get_the_id());

        $args = array(
            'post_type' => $post_type,
            'posts_per_page' => $posts_per_page,
            'exclude' => $exclude_posts,
        );

        if ($taxonomy && $terms) {
            $args['tax_query'] = array(
                array(
                    'taxonomy' => $taxonomy,
                    'field'    => 'slug',
                    'terms'    => explode(', ', $terms),
                ),
            );
        }
        $even_more_posts = get_posts($args);
        foreach ($even_more_posts as $post_object) {
            array_push($related_posts, $post_object);
        }
    }

    return $related_posts;
}

/**
 * Gets the post terms in hierarchical order
 *
 * @param  string  $visc_taxonomy   	Taxonomy for terms
 * @param  integer $id 				Post ID to check
 * @return array 			        Array of term objects
 */
function visc_get_hierarchical_post_terms($visc_taxonomy, $id)
{
    // Empty array we will fill with term objects
    global $output;
    $output = array();
    // ID of post
    if (empty($id)) {
        $id = $post->ID;
    }
    // Get post terms
    $terms = get_the_terms($id, $visc_taxonomy);
    if ($terms && !is_wp_error($terms)) {
        // create empy array for term ids
        global $post_terms_array;
        $post_terms_array = array();
        foreach ($terms as $term) {
            //  put term ids in array
            array_push($post_terms_array, $term->term_id);
        }

        function get_post_term_children($term_id, $term_taxonomy)
        {
            global $visc_taxonomy;
            global $post_terms_array;
            global $output;
            // Checks to see if the current term has children (not just specific to this post -- system-wide)
            if (get_term_children($term_id, $term_taxonomy)) {
                // Creates an array of the child term objects of the current term
                $child_terms = get_terms($term_taxonomy, array('parent' => $term_id));
                // creates an empty array for child term ids
                $post_child_terms_array = array();
                foreach ($child_terms as $child_term) {
                    //  puts child term ids in the array
                    // array_push($post_child_terms_array, $child_term->term_id);
                    if (in_array($child_term->term_id, $post_terms_array)) {
                        $term_obj = get_term_by('id', $child_term->term_id, $term_taxonomy);
                        array_push($output, $term_obj);
                    }
                    get_post_term_children($child_term->term_id, $term_taxonomy);
                }
            }
        }
        foreach ($terms as $term) {
            // Go through array objects again. Check if it's parent is a post term. If it isn't then we can add it and start the process
            if (!in_array($term->parent, $post_terms_array)) {
                array_push($output, $term);
                // Recursively calls function to go down hierarchical line
                get_post_term_children($term->term_id, $term->taxonomy);
            }
        }
    }
    return $output;
}

/**
 * Gets distinct values from a custom field
 * From http://raymondmoul.com/getting-distinct-values-from-acf-inputs/
 * 
 * @param  string  $meta_key   	    meta key to get values from
 * @return array 			        Array of meta values
 */
function visc_get_distinct_meta_values($meta_key)
{
    global $wpdb;
    $result = $wpdb->get_results($wpdb->prepare("
            SELECT DISTINCT meta_value
            FROM {$wpdb->postmeta} LEFT JOIN {$wpdb->posts}
            ON {$wpdb->posts}.ID = {$wpdb->postmeta}.post_id
            WHERE meta_key = '%s' AND meta_value != '' 
            AND post_type != 'revision'
            AND post_status = 'publish'
            ORDER BY meta_value DESC
        ", $meta_key));
    return $result;
}

/**
 * Takes a string and returns a truncated version. Also strips out shortcodes
 *
 * @param  string  $text   String to truncate
 * @param  integer $length Character count limit for truncation
 * @param  string  $append Appended to the end of the string if character count exceeds limit
 * @return string          Truncated string
 */
function visc_truncate_text($text = '', $length = 150, $append = '...')
{
    $new_text = preg_replace(" ([.*?])", '', $text);
    $new_text = strip_shortcodes($new_text);
    $new_text = strip_tags($new_text);
    $new_text = substr($new_text, 0, $length);
    if (strlen($new_text) == $length) {
        $new_text = substr($new_text, 0, strripos($new_text, " "));
        $new_text = $new_text . $append;
    }

    return $new_text;
}

/**
 * Visceral Get Terms List
 *
 * @param  integer $id   ID of the post
 * @param  string $taxonomy  taxonomy to get ters from
 * @param  string $sep  character to separate terms with
 * @return  A list of terms for a post (not linked)
 */
function visc_get_the_term_list($id, $taxonomy = '', $sep = ', ')
{
    if (is_null($id)) {
        $id = get_the_id();
    }

    // conditionally check for taxonomy 'type' by post type
    if (empty($taxonomy)) {
        $post_type = get_post_type($id);

        if ($post_type == 'post') {
            $taxonomy = 'news_type';
        } elseif ($post_type == 'resource') {
            $taxonomy = 'resource_type';
        }
    }

    $terms_list = get_the_term_list($id, $taxonomy, '', $sep, '');

    if (!is_wp_error($terms_list)) {
        return strip_tags($terms_list);
    } else {
        return false;
    }
}

/**
 * Returns output of post authors, based on ACF relationship or text field - NOT Wordpress author attribution
 * @param  integer $id Post ID
 * @param  boolean $link Apply link to author's post
 * @param  string $author_field ACF relationship field
 * @param  string $custom_authors ACF text field
 * 
 */
function visc_get_the_authors($id, $link = true, $author_field = 'authors', $custom_authors = 'custom_author')
{
    $authors_output = array();
    $authors        = (function_exists('get_field') && get_field($author_field, $id)) ? get_field($author_field, $id) : ''; // Relationship field, return post ID
    $custom_author  = get_post_meta($id, $custom_authors, true); // Custom Author Repeater field
    $image = '';

    if (!empty($authors)) {
        foreach ($authors as $author) {
            if (has_post_thumbnail($author)) {
                $image = get_the_post_thumbnail($author, 'thumbnail'); // returns a <picture> tag
            }

            if ($link) {
                $authors_output[] = '<div class="authors">' . $image . '<p><a href="' . get_the_permalink($author) . '">' . get_the_title($author) . '</a></p></div>';
            } else {
                $authors_output[] = '<div class="authors">' . $image . '<p>' . get_the_title($author) . '</p></div>';
            }
        }
    }

    // Custom Author Repeater field
    if ($custom_author) {
        $custom = '';
        while (have_rows($custom_authors)) : the_row();
            $image = get_sub_field('image');
            $name = get_sub_field('name');
            $title = get_sub_field('title');

            if ($image) {
                $custom_image = '<img class="cust-auth-img" src="' . $image['url'] . '" />';
            }

            $custom .= '<div class="authors">' . $custom_image . ' ' . $name . ' ' . $title . '</div>';
        endwhile;
        $authors_output[] = $custom;
    }

    if ($authors_output) {
        $authors_output = implode(' ', $authors_output);
    }

    return !empty($authors_output) ? $authors_output : '';
}

/**
 * Filter Input Builder
 * Returns markup of input to be used as filter for a custom query
 * @param  string $taxonomy Taxonomy to use for taxonomy term options
 * @param  string $type The type of input
 * @param  boolean $hidden Whether the input should be hidden from user or not
 * @param  boolean $auto_submit Whether the input should auto-submit or not
 * @param  boolean $reverse Whether the taxonomy terms should be in reverse order or not
 * 
 */
function visc_form_filter_input($taxonomy, $type = 'select', $hidden = false, $auto_submit = false, $reverse = false)
{

    $tax_obj = get_taxonomy($taxonomy);
    $form_filters = Controllers\App::formFilters();
    if ($auto_submit) {
        $auto_submit = 'onchange="this.form.submit()"';
    }
    $markup = '';
    $terms = get_terms($taxonomy);
    if ($reverse) {
        $terms = array_reverse($terms);
    }
    if (!empty($terms) && !is_wp_error($terms)) {
        if ($type === 'checkbox') {
            $markup .= '<fieldset class="' . ($hidden ? 'screen-reader-text' : '') . '">';
            $markup .= '<legend class="screen-reader-text">' . $tax_obj->labels->singular_name . '</legend>';
            foreach ($terms as $term) :
                $markup .= '<label for="' . $term->slug . '">';
                $markup .= '<input type="checkbox" id="' . $term->slug . '" value="' . $term->slug . '" name="tax_filter_' . $taxonomy . '[]"';
                if (!is_null($form_filters->{'tax_filter_' . $taxonomy})) {
                    if (in_array($term->slug, $form_filters->{'tax_filter_' . $taxonomy})) {
                        $markup .= ' checked';
                    }
                }
                $markup .= '>';
                $markup .= '<span>' . $term->name . '</span>';
                $markup .= '</label>';
            endforeach;
            $markup .= '</fieldset>';
        } else {
            $markup .= '<label for="' . $taxonomy . '" class="' . ($hidden ? 'screen-reader-text' : '') . '">';
            $markup .= '<span class="screen-reader-text">' . $tax_obj->labels->singular_name . '</span>';

            $markup .= '<select name="tax_filter_' . $taxonomy . '" id="' . $taxonomy . '" ' . $auto_submit . '><option value="">' . __('All ', 'visceral') . $tax_obj->label . '</option>';
            foreach ($terms as $term) :
                $markup .= '<option value="' . $term->slug . '"';
                if (!is_null($form_filters->{'tax_filter_' . $taxonomy})) {
                    if ($form_filters->{'tax_filter_' . $taxonomy} == $term->slug) {
                        $markup .= ' selected="selected"';
                    }
                }
                $markup .= '>' . $term->name . '</option>';
            endforeach;
            $markup .= '</select>';

            $markup .= '</label>';
        }
    }

    return $markup;
}

/**
 * visc_get_all_fields
 * Returns all ACF field data for a given post
 * @param  integer $post_id ID of the post
 * 
 */
function visc_get_all_fields($post_id)
{
    $fields = false;

    // Check to see if ACF is installed
    if (function_exists('get_fields')) {

        // Get fields from ACF
        $fields = get_fields($post_id);

        // Create Empty fields array
        $post_fields = array();

        if ($fields) {
            foreach ($fields as $name => $value) {
                // If field value is a string, set it
                if (is_string($value) || is_bool($value)) {
                    $post_fields[$name] = $value;
                }
                // If field value is an array
                if (is_array($value)) {
                    // If it's an assoc. array
                    if (count(array_filter(array_keys($value), 'is_string')) > 0) {
                        // Get sub names and set them as properties and values
                        foreach ($value as $sub_key => $sub_value) {
                            $post_fields[$sub_key] = $sub_value;
                        }
                        // Else it's a simple array
                    } else {
                        foreach ($value as $sub_value) {
                            // If the values within this array are arrays, go deeper!
                            if (is_array($sub_value)) {
                                foreach ($sub_value as $sub_sub_name => $sub_sub_value) {
                                    $post_fields[$sub_sub_name] = $sub_sub_value;
                                }
                                // Otherwise just grab the value and assign it to the top key
                            } else {
                                $post_fields[$name] = $sub_value;
                            }
                        }
                    }
                }
            }
        }
    }

    return $post_fields;
}

/**
 * Visceral Breadcrumbs
 * Returns a breadcrumb link for a given page
 * 
 */
function visc_breadcrumbs()
{
    $link_id = '';
    if (is_front_page()) {
        return;
    }
    if (is_single()) {
        return;
    }

    $link_id = wp_get_post_parent_id(get_the_id());

    // Top Level Page
    if ($link_id === 0) {
        $link_id = get_option('page_on_front');
    }

    return '<a href="' . get_permalink($link_id) . '" class="breadcrumb">' . get_the_title($link_id) . '</a>';
}
