<?php
if (isset($data)) {
    $ID = $data->ID;
} else {
    $ID = get_the_id();
}

$image = get_the_post_thumbnail($ID, 'medium', array('class' => 'img-crop'));
$job_title = get_post_meta($ID, 'job_title', true);
$organization = get_post_meta($ID, 'organization', true);
?>

<div class="col col-12 col-sm-6 col-md-3 <?php echo (isset($col_class)) ? $col_class : ''; ?>">
    <article class="list-item-person">
        <?php
        if ($image) :
            ?>
        <a href="{{ get_permalink($ID)}}" class="list-item-person__image">    
        <?php echo $image; ?>    
        <?php endif; ?>
        <div class="list-item-person__main text-center">
            <h1 class="list-item-person__title">
                <a href="{{ get_permalink($ID)}}"><?php echo get_the_title($ID); ?></a>
            </h1>
            <?php
            if ($job_title) :
                ?>
            <div class="list-item-person__job-title">
                <p><?php echo $job_title; ?></p>
            </div>
            <?php endif; ?>
            <?php
            if ($organization) :
                ?>
            <div class="list-item-person__organization">
                <p class="small"><?php echo $organization; ?></p>
            </div>
            <?php endif; ?>
        </div>
    </article>
</div>