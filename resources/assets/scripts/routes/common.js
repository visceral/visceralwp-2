import gsap from 'gsap';
import ScrollTrigger from 'gsap/ScrollTrigger';
import 'slick-carousel';
import 'magnific-popup';

export default {
    links() {
        /** SKIP LINK NAVIGATION */
        $('#skip-to-content').click(function () {
            // strip the leading hash and declare
            // the content we're skipping to
            var skipTo = '#' + this.href.split('#')[1];
            // Setting 'tabindex' to -1 takes an element out of normal
            // tab flow but allows it to be focused via javascript
            $(skipTo)
                .attr('tabindex', -1)
                .on('blur focusout', function () {
                    // when focus leaves this element,
                    // remove the tabindex attribute
                    $(this).removeAttr('tabindex');
                })
                .focus(); // focus on the content container
        });
        /** END SKIP LINK NAVIGATION */

        /** ANIMATED ANCHOR LINKS **/
        $('a[href*="#"]:not([href="#"])').click(function (e) {
            if (
                location.pathname.replace(/^\//, '') ===
                    this.pathname.replace(/^\//, '') &&
                location.hostname === this.hostname
            ) {
                var target = $(this.hash);
                var $this = this;
                var header = $('header.site-header');
                var wpAdminBar = $('#wpadminbar');
                var fixedHeaderOffset =
                    parseInt(header.outerHeight()) -
                    parseInt(header.css('padding-top').replace('px', ''));
                // If we're logged in and WP ADmin Bar exists, add it to the offset
                if (wpAdminBar.length) {
                    fixedHeaderOffset += parseInt(wpAdminBar.outerHeight());
                }
                target = target.length
                    ? target
                    : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate(
                        {
                            scrollTop: target.offset().top - fixedHeaderOffset,
                        },
                        500,
                        function () {
                            if (history.pushState) {
                                history.pushState(null, null, $this.hash);
                            } else {
                                location.hash = $this.hash;
                            }
                        }
                    );
                    e.preventDefault();
                }
            }
        });
        /** END ANIMATED ANCHOR LINKS **/

        /** Break link text to new line when link text is a URL **/
        $(document).ready(function () {
            $('a').each(function () {
                var $this = $(this);
                var innerLink = $this.text();
                if (innerLink.startsWith('http')) {
                    $this.css('word-break', 'break-all');
                }
            });
        });
        /** End Break link text to new line when link text is a URL **/
    },
    itemReveal() {
        // eslint-disable-next-line no-unused-vars
        // var $itemReveals = $('.reveal').waypoint({
        //   handler: function () {
        //     $(this)[0].element.classList.add('revealed');
        //   },
        //   offset: '85%',
        // });

        // Use gsap and ScrollTrigger
        gsap.registerPlugin(ScrollTrigger);
        let reveals = gsap.utils.toArray('.reveal');
        reveals.forEach((reveal) => {
            ScrollTrigger.create({
                trigger: reveal,
                start: 'top 75%', // when the top of the element is 75% from the top of the viewport
                toggleClass: 'revealed',
                // toggleActions: 'play pause resume none',
                once: true, // only trigger the toggle once. we can control repeat behavior using toggleActions
            });
        });

        // Stagger reveals with a gsap.timeline() + ScrollTrigger
        let staggers = gsap.utils.toArray('.reveal--staggered');
        staggers.forEach((stagger) => {
            let tl = gsap.timeline({
                scrollTrigger: {
                    trigger: stagger,
                    start: 'top 75%',
                },
            });

            let children = stagger.children; // the immediate children elements of the current target

            tl.to(children, {
                duration: 1,
                y: 0,
                opacity: 1,
                stagger: 0.15,
            });
        });
    },
    slider() {
        let visceralSlider = $('.visceral-slider');

        if (visceralSlider) {
            visceralSlider.each(function () {
                console.log($(this));
                let slider, slide, slidesToShow, centerMode;
                slider = $(this).find(
                    '> .elementor-column-wrap > .elementor-widget-wrap'
                );
                slide =
                    '.visceral-slider > .elementor-column-wrap > .elementor-widget-wrap > *';

                slidesToShow = 1;
                centerMode = true;

                if ($(this).hasClass('posts-list-container')) {
                    slider = $('.visceral-slider .row');
                    slide = '.visceral-slider .row > *';
                    slidesToShow = visceralSlider.attr('data-slides-to-show');
                    centerMode = false;
                }

                slider.slick({
                    slide: slide,
                    autoplay: false,
                    fade: false,
                    speed: 1200,
                    infinite: true,
                    cssEase: 'ease',
                    centerMode: false,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    swipeToSlide: true,
                    dots: true,
                    arrows: true,
                    mobileFirst: true,
                    rows: 0,
                    nextArrow:
                        '<button type="button" class="slick-next"><span class="screen-reader-text">Next</span><i class="icon-chevron-right"></i></button>',
                    prevArrow:
                        '<button type="button" class="slick-prev"><span class="screen-reader-text">Previous</span><i class="icon-chevron-left"></i></button>',
                    responsive: [
                        {
                            breakpoint: 560, // min-width
                            settings: {
                                slidesToShow: 2,
                                centerMode: centerMode,
                            },
                        },
                        {
                            breakpoint: 992, // min-width
                            settings: {
                                slidesToShow: slidesToShow,
                            },
                        },
                    ],
                });
            });
        }
    },
    magnificPopup() {
        let videoLink = $('.modal-link-video');
        if (videoLink) {
            videoLink.magnificPopup({
                disableOn: 700,
                type: 'iframe',
                mainClass: 'mfp-fade',
                removalDelay: 160,
                preloader: false,
                fixedContentPos: false,
            });
        }
    },
    masthead() {
        /** MASTHEAD PRGRESSIVE IMAGES **/
        var masthead = document.querySelector('.masthead'),
            placeholderOverlay = document.querySelector('.masthead__overlay');

        if (masthead && placeholderOverlay) {
            // Load full size image. When loaded, fade our placeholder add it as bg to masthead
            var img = new Image();
            img.src = masthead.dataset.imageSrc;
            img.onload = function () {
                placeholderOverlay.classList.add('fade-out');
                masthead.style.backgroundImage = 'url(' + img.src + ')';
            };
        }
        /** END MASTHEAD PRGRESSIVE IMAGES **/
    },
    header() {
        /** FIXED HEADER ANIMATION **/
        var didScroll = false,
            scrollOffset = 100;

        function scrollY() {
            return window.pageYOffset || document.documentElement.scrollTop;
        }

        function scrollPage() {
            var sy = scrollY();
            if (sy > scrollOffset) {
                $('.site-header--fixed').addClass(
                    'site-header--scroll-triggered'
                );
            } else {
                $('.site-header--fixed').removeClass(
                    'site-header--scroll-triggered'
                );
            }
            didScroll = false;
        }

        window.addEventListener('load', scrollPage);
        window.addEventListener(
            'scroll',
            function () {
                // eslint-disable-line no-unused-vars
                if (!didScroll) {
                    didScroll = true;
                    setTimeout(scrollPage, 100);
                }
            },
            false
        );
        /** END FIXED HEADER ANIMATION **/

        /**  SEARCH OVERLAY **/
        const searchIcon = document.querySelector('.site-header__search-icon');
        const siteSearch = document.querySelector('.site-search');
        const siteSearchClose = document.querySelector('.site-search__close');

        searchIcon.addEventListener('click', (e) => {
            e.preventDefault();
            siteSearch.classList.add('site-search--active');
            setTimeout(() => {
                $('.site-search__form input[type="text"]').focus();
            }, 500);
        });
        siteSearchClose.addEventListener('click', () => {
            siteSearch.classList.remove('site-search--active');
        });
        /**  END SEARCH OVERLAY **/

        // Mobile Icon
        $('.mobile-nav-icon').on('click', function () {
            $(this).toggleClass('mobile-nav-icon--active');
            if ($(this).attr('aria-expanded') === 'true') {
                $(this).attr('aria-expanded', 'false');
            } else {
                $(this).attr('aria-expanded', 'true');
            }

            $('.site-header').toggleClass('site-header--mobile-nav-active');
            $('body').toggleClass('mobile-nav-active');
            $('.mobile-nav-container').toggleClass(
                'mobile-nav-container--active'
            );
        });

        // Disable click on mobile
        $('.mobile-menu__item--parent .sub-menu-toggle').on(
            'click',
            function () {
                $(this)
                    .parent('.mobile-menu__item')
                    .toggleClass('mobile-menu__item--opened');
            }
        );
    },
    mainNavAccessibility() {
        let $mainNavigationTopLevelItemLinks = $(
            '.site-header .main-menu > .main-menu__item--parent > button'
        );

        if ($mainNavigationTopLevelItemLinks.length) {
            $mainNavigationTopLevelItemLinks.each(function () {
                $(this).prev('a').attr('aria-haspopup', 'true');
                $(this).on('click', function () {
                    $(this).closest('li').toggleClass('sub-menu-open');
                    $(this).attr('aria-expanded', 'true');
                });
            });
        }
    },
    ajaxLoadMore() {
        // AJAX Load More
        function visc_load_more() {
            // eslint-disable-next-line no-undef
            if (visceral_vars !== undefined) {
                var data = {
                    action: 'visc_load_more',
                    // eslint-disable-next-line no-undef
                    query: visceral_vars.query,
                    // eslint-disable-next-line no-undef
                    paged: visceral_vars.current_page,
                    // eslint-disable-next-line no-undef
                    security: visceral_vars.security,
                };

                var $button = $('.btn--load-more');

                $.ajax({
                    // eslint-disable-next-line no-undef
                    url: visceral_vars.ajaxurl, // AJAX handler
                    data: data,
                    type: 'POST',
                    beforeSend: function () {
                        $button.find('span').text('Loading...'); // change the button text, you can also add a preloader image
                    },
                    complete: function () {
                        this.itemReveal();
                    },
                    success: function (data) {
                        $button.blur();
                        if (data) {
                            $button.find('span').text('Load More');

                            var $postsContainer = $(
                                '.posts-list-container > div'
                            );
                            $postsContainer.append(data);
                            // eslint-disable-next-line no-undef
                            visceral_vars.current_page++;
                            // eslint-disable-next-line no-undef
                            if (
                                // eslint-disable-next-line no-undef
                                visceral_vars.current_page ==
                                // eslint-disable-next-line no-undef
                                visceral_vars.max_page
                            ) {
                                $button.remove(); // if last page, remove the button
                            }
                        } else {
                            $button.remove(); // if last page, remove the button
                        }
                    },
                });
            }
        }

        $('.btn--load-more').on('click', function () {
            visc_load_more();
        });
    },
    socialShare() {
        /**  SOCIAL SHARE OVERLAY **/
        const shareLink = document.querySelector('.visc-social-share-btn');
        const socialShare = document.querySelector('.visc-social-share');
        const socialShareClose = document.querySelector(
            '.visc-social-share__close'
        );

        if (socialShare) {
            shareLink.addEventListener('click', () => {
                // console.log(shareLink);
                socialShare.classList.add('visc-social-share--active');
            });
            socialShareClose.addEventListener('click', () => {
                socialShare.classList.remove('visc-social-share--active');
            });
        }
        /**  END SOCIAL SHARE OVERLAY **/
    },
    init() {
        // JavaScript to be fired on all pages

        /** GLOBAL VARIABLES */
        // var body = document.querySelector('body');
        /** END GLOBAL VARIABLES */

        // Misc. link behaviors
        this.links();

        // Global reveal animations (consider commenting out this and 'gsap' and 'ScrollTrigger' import if not in use)
        this.itemReveal();

        // Slider (consider commenting out this and 'slick-carousel' import if not in use)
        this.slider();

        // Modals (consider commenting out this and 'magnific-popup' import if not in use)
        this.magnificPopup();

        // Masthead
        this.masthead();

        // Header
        this.header();

        // Main Nav Accessibility
        this.mainNavAccessibility();

        // Load more
        this.ajaxLoadMore();

        // Social share
        this.socialShare();
    },
    finalize() {
        // JavaScript to be fired on all pages, after page specific JS is fired
    },
};
