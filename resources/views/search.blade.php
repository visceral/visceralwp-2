@extends('layouts.app')
@section('masthead')
    @include('partials.masthead-search')
@endsection
@section('content')
    <div class="container">
        @if (have_posts())
            @while(have_posts()) @php the_post() @endphp
                @include('partials.list-item-search')
            @endwhile
        @else
            <div class="m-t-40 lg-m-t-80 m-b-40 lg-m-b-80">
                <h4>{{ __('Sorry, no results were found.', 'sage') }}</h4>
            </div>
        @endif

        <div class="pagination">
            {!! $pagination !!}
        </div>
    </div>
@endsection