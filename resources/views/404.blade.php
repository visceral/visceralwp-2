@extends('layouts.app')

@section('content')
@if (!have_posts())
<div class="error404__content overlay" style="background-image: url({{ $background_image }});">
    <div class="container">
        <div class="text-center text-white">
            <h1>{{ __('Page not found.', 'visceral')}}</h1>
            <p class="h6">{{ $description }}</p>
            <a href="{{ site_url() }}" class="btn">Back to Home</a>
        </div>
    </div>
</div> @endif @endsection