@extends('layouts.app')

@section('content')
<div class="container">
    @while(have_posts()) @php(the_post())
    @section('masthead')
    @include('partials.masthead')
    @endsection
    <section class="post-content">
        <h1 class="sg-heading">Colors</h1>

        <div class="sg-element">
            <div class="row">
                <div class="col col-12 col-md-3 text-center">
                    <div class="sg-color-block" style="background: #03061b;"></div>
                    <p class="sg-sub-heading">Primary Base</p>
                </div>
                <div class="col col-12 col-md-3 text-center">
                    <div class="sg-color-block" style="background: #81828d;"></div>
                    <p class="sg-sub-heading">Dark Gray</p>
                </div>
                <div class="col col-12 col-md-3 text-center">
                    <div class="sg-color-block" style="background: #c0c1c6;"></div>
                    <p class="sg-sub-heading">Medium Gray</p>
                </div>
                <div class="col col-12 col-md-3 text-center">
                    <div class="sg-color-block" style="background: #f2f2f4;"></div>
                    <p class="sg-sub-heading">Light Gray</p>
                </div>
                <div class="col col-12 col-md-3 text-center">
                    <div class="sg-color-block" style="background: #062db3;"></div>
                    <p class="sg-sub-heading">Primary Highlight</p>
                </div>
                <div class="col col-12 col-md-3 text-center">
                    <div class="sg-color-block" style="background: #4a78ff;"></div>
                    <p class="sg-sub-heading">Secondary 1</p>
                </div>
                <div class="col col-12 col-md-3 text-center">
                    <div class="sg-color-block" style="background: #61dbff;"></div>
                    <p class="sg-sub-heading">Secondary 2</p>
                </div>
                <div class="col col-12 col-md-3 text-center">
                    <div class="sg-color-block" style="background: #35c7a1;"></div>
                    <p class="sg-sub-heading">Secondary 3</p>
                </div>
                <div class="col col-12 col-md-3 text-center">
                    <div class="sg-color-block" style="background: #d0cb52;"></div>
                    <p class="sg-sub-heading">Secondary 4</p>
                </div>
            </div>
        </div>
        <h1 class="sg-heading">Elements</h1>

        <div class="sg-element">
            <div class="row">
                <div class="col col-12 col-md-3">
                    <p class="sg-sub-heading">Heading 1</p>
                </div>
                <div class="col col-12 col-md-9">
                    <h1>Heading 1</h1>
                </div>
            </div>
        </div>
        <div class="sg-element">
            <div class="row">
                <div class="col col-12 col-md-3">
                    <p class="sg-sub-heading">Heading 2</p>
                </div>
                <div class="col col-12 col-md-9">
                    <h2>Heading 2</h2>
                </div>
            </div>
        </div>
        <div class="sg-element">
            <div class="row">
                <div class="col col-12 col-md-3">
                    <p class="sg-sub-heading">Heading 3</p>
                </div>
                <div class="col col-12 col-md-9">
                    <h3>Heading 3</h3>
                </div>
            </div>
        </div>
        <div class="sg-element">
            <div class="row">
                <div class="col col-12 col-md-3">
                    <p class="sg-sub-heading">Heading 4</p>
                </div>
                <div class="col col-12 col-md-9">
                    <h4>Heading 4</h4>
                </div>
            </div>
        </div>
        <div class="sg-element">
            <div class="row">
                <div class="col col-12 col-md-3">
                    <p class="sg-sub-heading">Heading 5</p>
                </div>
                <div class="col col-12 col-md-9">
                    <h5>Heading 5</h5>
                </div>
            </div>
        </div>
        <div class="sg-element">
            <div class="row">
                <div class="col col-12 col-md-3">
                    <p class="sg-sub-heading">Heading 6</p>
                </div>
                <div class="col col-12 col-md-9">
                    <h6>Heading 6</h6>
                </div>
            </div>
        </div>
        <div class="sg-element">
            <div class="row">
                <div class="col col-12 col-md-3">
                    <p class="sg-sub-heading">Heading with Buffer</p>
                    <p class="sg-sub-heading">Paragraph with Link</p>
                </div>
                <div class="col col-12 col-md-9">
                    <h2 class="buffer">Heading with Buffer</h2>
                    <p>Paragraph Lorem ipsum dolor, sit amet <a href="#">Link consectetur</a> adipisicing elit. Eum, eos
                        neque,
                        exercitationem rem incidunt quis delectus, adipisci quisquam laboriosam nulla officia autem
                        architecto
                        molestias
                        vitae vero nam! Harum, quidem dignissimos?</p>
                </div>
            </div>
        </div>
        <div class="sg-element">
            <div class="row">
                <div class="col col-12 col-md-3">
                    <p class="sg-sub-heading">Paragraph (lead-in)</p>
                </div>
                <div class="col col-12 col-md-9">
                    <p class="lead">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Saepe reprehenderit
                        voluptas.
                    </p>
                </div>
            </div>
        </div>
        <div class="sg-element">
            <div class="row">
                <div class="col col-12 col-md-3">
                    <p class="sg-sub-heading">Paragraph (small)</p>
                </div>
                <div class="col col-12 col-md-9">
                    <p class="small">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Saepe reprehenderit
                        voluptas.
                    </p>
                </div>
            </div>
        </div>
        <div class="sg-element">
            <div class="row">
                <div class="col col-12 col-md-3">
                    <p class="sg-sub-heading">Unordered List</p>
                </div>
                <div class="col col-12 col-md-9">
                    <ul>
                        <li>adipisci quisquam laboriosam nulla officia</li>
                        <li>arum, quidem dignissimos</li>
                        <li>laboriosam nulla</li>
                        <li>sit amet <a href="#">Link consectetur</a> adipisicing elit</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="sg-element">
            <div class="row">
                <div class="col col-12 col-md-3">
                    <p class="sg-sub-heading">Ordered List</p>
                </div>
                <div class="col col-12 col-md-9">
                    <ol>
                        <li>adipisci quisquam laboriosam nulla officia</li>
                        <li>arum, quidem dignissimos</li>
                        <li>laboriosam nulla</li>
                        <li>sit amet <a href="#">Link consectetur</a> adipisicing elit</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="sg-element">
            <div class="row">
                <div class="col col-12 col-md-3">
                    <p class="sg-sub-heading">Blockquote</p>
                </div>
                <div class="col col-12 col-md-9">
                    <blockquote>Blockquote Lorem ipsum dolor sit amet consectetur adipisicing elit.</blockquote>
                </div>
            </div>
        </div>
        <div class="sg-element">
            <div class="row">
                <div class="col col-12 col-md-3">
                    <p class="sg-sub-heading">Blockquote with citation</p>
                </div>
                <div class="col col-12 col-md-9">
                    <blockquote>
                        <p>
                            Blockquote dolor sit amet consectetur adipisicing elit.
                        </p>
                        <cite>Citation Name</cite>
                    </blockquote>
                </div>
            </div>
        </div>
        <div class="sg-element">
            <div class="row">
                <div class="col col-12 col-md-3">
                    <p class="sg-sub-heading">Image with Caption</p>
                </div>
                <div class="col col-12 col-md-9">
                    <figure>
                        <img src="https://via.placeholder.com/300x200" alt="">
                        <figcaption>Image caption lorem ipsum dolor</figcaption>
                    </figure>
                </div>
            </div>
        </div>
        <div class="sg-element">
            <div class="row">
                <div class="col col-12 col-md-3">
                    <p class="sg-sub-heading">Shadow</p>
                </div>
                <div class="col col-12 col-md-9">
                    <figure>
                        <img class="visc-shadow" src="https://via.placeholder.com/300x200" alt="">
                    </figure>
                </div>
            </div>
        </div>
        <div class="sg-element">
            <div class="row">
                <div class="col col-12 col-md-3">
                    <p class="sg-sub-heading">Table</p>
                </div>
                <div class="col col-12 col-md-9">
                    <table>
                        <tbody>
                            <tr>
                                <th>Table heading</th>
                                <th>Table heading</th>
                                <th>Table heading</th>
                                <th>Table heading</th>
                            </tr>
                            <tr>
                                <td>Table data</td>
                                <td>Table data</td>
                                <td>Table data</td>
                                <td>Table data</td>
                            </tr>
                            <tr>
                                <td>Table data</td>
                                <td>Table data</td>
                                <td>Table data</td>
                                <td>Table data</td>
                            </tr>
                            <tr>
                                <td>Table data</td>
                                <td>Table data</td>
                                <td>Table data</td>
                                <td>Table data</td>
                            </tr>
                            <tr>
                                <td>Table data</td>
                                <td>Table data</td>
                                <td>Table data</td>
                                <td>Table data</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="sg-element">
            <div class="row">
                <div class="col col-12 col-md-3">
                    <p class="sg-sub-heading">Horizontal Rule</p>
                </div>
                <div class="col col-12 col-md-9">
                    <hr>
                </div>
            </div>
        </div>
        <div class="sg-element">
            <div class="row">
                <div class="col col-12 col-md-3">
                    <p class="sg-sub-heading">Horizontal Rule (White)</p>
                </div>
                <div class="col col-12 col-md-9">
                    <div class="sg-dark-bg">
                        <hr class="hr--white">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <h1 class="sg-heading">Components</h1>
        <div class="sg-element">
            <div class="row">
                <div class="col col-12 col-md-3">
                    <p class="sg-sub-heading">Buttons</p>
                </div>
                <div class="col col-12 col-md-9">
                    <p>
                        <a href="#" class="btn">Default Button</a>
                        <a href="#" class="btn btn--secondary1">Secondary 1 Button</a>
                        <a href="#" class="btn btn--secondary2">Secondary 2 Button</a>
                    </p>
                    </p>
                    <a href="#" class="btn btn--secondary3">Secondary 3 Button</a>
                    <a href="#" class="btn btn--secondary4">Secondary 4 Button</a>
                    </p>
                    <div class="sg-dark-bg">
                        <a href="#" class="btn btn--white">White Button</a>

                    </div>
                </div>
            </div>
        </div>
        <div class="sg-element">
            <div class="row">
                <div class="col col-12 col-md-3">
                    <p class="sg-sub-heading">Eye Brow</p>
                </div>
                <div class="col col-12 col-md-9">
                    <h2 class="eyebrow">Eyebrow</h2>
                    <h4>Lorem ipsum title</h4>
                </div>
            </div>
        </div>
        <div class="sg-element">
            <div class="row">
                <div class="col col-12 col-md-3">
                    <p class="sg-sub-heading">Stat</p>
                </div>
                <div class="col col-12 col-md-9">
                    <p class="visc-stat">25</p>
                </div>
            </div>
        </div>
        <div class="sg-element">
            <div class="row">
                <div class="col col-12 col-md-3">
                    <p class="sg-sub-heading">Ticker Stat</p>
                </div>
                <div class="col col-12 col-md-9">
                    <p class="visceral-ticker">500</p>
                </div>
            </div>
        </div>
        <form action="">
            <div class="sg-element">
                <div class="row">
                    <div class="col col-12 col-md-3">
                        <p class="sg-sub-heading">Form - Text Inut</p>
                    </div>
                    <div class="col col-12 col-md-9">
                        <label for="text">Text Label</label>
                        <input type="text">
                    </div>
                </div>
            </div>
            <div class="sg-element">
                <div class="row">
                    <div class="col col-12 col-md-3">
                        <p class="sg-sub-heading">Form - Textarea</p>
                    </div>
                    <div class="col col-12 col-md-9">
                        <label for="textarea">Textarea Label</label>
                        <textarea name="textarea" id="textarea" cols="30" rows="10"></textarea>
                    </div>
                </div>
            </div>
            <div class="sg-element">
                <div class="row">
                    <div class="col col-12 col-md-3">
                        <p class="sg-sub-heading">Form - Select</p>
                    </div>
                    <div class="col col-12 col-md-9">
                        <label for="select">Select Label</label>
                        <select name="select" id="select">
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="sg-element">
                <div class="row">
                    <div class="col col-12 col-md-3">
                        <p class="sg-sub-heading">Form - checkbox</p>
                    </div>
                    <div class="col col-12 col-md-9">
                        <label for="checkbox-1"><input id="checkbox-1" type="checkbox"> Checkbox Label </label>
                        <label for="checkbox-2"><input id="checkbox-2" type="checkbox"> Checkbox Label </label>
                        <label for="checkbox-3"><input id="checkbox-3" type="checkbox"> Checkbox Label </label>
                    </div>
                </div>
            </div>
            <div class="sg-element">
                <div class="row">
                    <div class="col col-12 col-md-3">
                        <p class="sg-sub-heading">Form - radio</p>
                    </div>
                    <div class="col col-12 col-md-9">
                        <label for="radio-1"><input id="radio-1" type="radio"> Radio Label </label>
                        <label for="radio-2"><input id="radio-2" type="radio"> Radio Label </label>
                        <label for="radio-3"><input id="radio-3" type="radio"> Radio Label </label>
                    </div>
                </div>
            </div>
            <div class="sg-element">
                <div class="row">
                    <div class="col col-12 col-md-3">
                        <p class="sg-sub-heading">Form - Submit</p>
                    </div>
                    <div class="col col-12 col-md-9">
                        <button type="submit">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </section>
    <section>
        {{ the_content()}}
    </section>
</div>
@endwhile
</div>
@endsection