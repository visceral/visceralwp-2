<!doctype html>
<html @php language_attributes() @endphp class="no-js">
@include('partials.head')

<body @php body_class() @endphp role="document">
    <div class="pre-loader" id="pre-loader"></div>
    <a id="skip-to-content" href="#main-content" class="no-print"
        aria-label="{{ __('Skip to content', 'visceral') }}">{{ __('Skip to content', 'visceral') }}</a>
    @php do_action('get_header') @endphp
    @include('partials.header')
    @yield('masthead')
    <main class="main" id="main-content" tabindex="-1">
        @yield('content')
    </main>
    @if (App\display_sidebar())
    <aside class="sidebar">
        @include('partials.sidebar')
    </aside>
    @endif
    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
</body>
<script type="text/javascript" defer>
    window.onload = function() {
        document.getElementById('pre-loader').classList.add('fade-out');
    };
</script>
</html>