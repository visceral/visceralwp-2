<div {{ post_class('list-item-search') }}>
    <p class="small list-item-search__label"><strong>{{ get_post_type() }}</strong></p>
    <h4 class="list-item-search__title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h4>
    <time class="list-item-search__time small" datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time>
</div>