@php
/**
* Masthead
* Single resource
*/
@endphp

<div class="masthead masthead--resource overlay {{ $masthead_image->img_class }}"
    data-image-src="{{ $masthead_image->full_size_image_url }}">
    @if ( $masthead_image->placeholder_image_url != '' )
    <span class="masthead__overlay img-bg"
        style="background-image: url({{$masthead_image->placeholder_image_url}});"></span>
    @endif
    <div class="container">
        <div class="masthead__content text-center">
            <div class="row justify-center">
                <div class="col col-md-10">
                    @if($types)
                    <p class="masthead__label">{!! $types !!} </p>
                    @endif
                    <h1 class="masthead__title">{!! App::title() !!}</h1>
                </div>
            </div>
        </div>
    </div>
</div>