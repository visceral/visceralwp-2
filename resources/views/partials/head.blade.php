<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script>
        // Prevents flash of unstyle cotnent for JS manipulated elements.
		document.querySelector('html').classList.remove('no-js');
    </script>
    @php wp_head() @endphp
</head>