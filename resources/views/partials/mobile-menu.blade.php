<div id="mobile-nav" class="mobile-nav-container">
    <div class="container-fluid mobile-nav-inner">
        @if(has_nav_menu('mobile_menu'))
        {!! App\visc_bem_menu('mobile_menu', 'mobile-menu') !!}
        @elseif(has_nav_menu('primary_menu'))
        {!! App\visc_bem_menu('primary_menu', 'mobile-menu') !!}
        @endif
        <form role="search" class="mobile-nav__search-form searchform" method="get" action="{{ site_url() }}">
            <label for="mobile-s">
                <input type="text" name="s" id="mobile-s" placeholder="{{ __('Search everything', 'visceral') }}"
                    autocomplete="off" spellcheck="false">
                <span class="screen-reader-text">
                    <?php _e('Keyword search', 'visceral'); ?>
                </span>
            </label>
            <button type="submit"><span class="screen-reader-text">
                    <?php _e('Submit', 'visceral'); ?>
                </span><i class="icon-search-icon"></i></button>
        </form>
    </div>
</div>