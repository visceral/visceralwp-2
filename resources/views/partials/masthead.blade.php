@php
/**
* Masthead
* Default masthead template
*
* To override on specifc pages or posts, use
* separate partials ('masthead-single') and
* include on their relative blade partial
*
*/
@endphp

<div class="masthead {{ $masthead_image->img_class }}" data-image-src="{{ $masthead_image->full_size_image_url }}">
    {{-- The low quality placeholder --}}
    @if ( $masthead_image->placeholder_image_url != '' )
    <span class="masthead__overlay img-bg"
        style="background-image: url({{$masthead_image->placeholder_image_url}});"></span>
    @endif
    <div class="container">
        <div class="masthead__content">
            <h1>{!! App::title() !!}</h1>
        </div>
    </div>
</div>