@php
/**
* Masthead
* Single post
*/
$post_type = App\visc_get_the_term_list($ID, 'news_type', ', ');
@endphp

<div class="masthead masthead--post overlay {{ $masthead_image->img_class }}"
    data-image-src="{{ $masthead_image->full_size_image_url }}">
    @if ( $masthead_image->placeholder_image_url != '' )
    <span class="masthead__overlay img-bg"
        style="background-image: url({{$masthead_image->placeholder_image_url}});"></span>
    @endif
    <div class="container">
        <div class="masthead__content text-center">
            <div class="row justify-center">
                <div class="col col-md-10">
                    {{-- @if($post_type) --}}
                    <p class="masthead__label">{!! $post_type !!}</p>
                    {{-- @endif --}}
                    <h1 class="masthead__title">{!! App::title() !!}</h1>
                    @include('partials.entry-meta')
                </div>
            </div>
        </div>
    </div>
</div>