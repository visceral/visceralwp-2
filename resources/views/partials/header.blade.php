<header class="site-header {{ $masthead_image->header_class }} {{ $header_options}} site-header--fixed">
    <div class="site-header__inner container">
        <a class="site-header__brand" href="{{ home_url('/') }}"><span
                class="screen-reader-text">{{ get_bloginfo('name', 'display') }}</span>
                @include('graphics.logo')
            </a>
        @include('partials.main-menu')
        <a class="site-header__search-icon" href="{{ site_url() }}/?s=" aria-label="Search"><span
                class="screen-reader-text">{{__('Search', 'visceral')}}</span><svg width="11" height="11"
                viewBox="0 0 11 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                    d="M6.62981 6.63582C7.15866 6.10697 7.42308 5.47196 7.42308 4.73077C7.42308 3.98958 7.15966 3.35557 6.63281 2.82873C6.10597 2.30188 5.47196 2.03846 4.73077 2.03846C3.98958 2.03846 3.35557 2.30188 2.82873 2.82873C2.30188 3.35557 2.03846 3.98958 2.03846 4.73077C2.03846 5.47196 2.30188 6.10597 2.82873 6.63281C3.35557 7.15966 3.98958 7.42308 4.73077 7.42308C5.47196 7.42308 6.10497 7.16066 6.62981 6.63582ZM10.5 9.73077C10.5 9.9391 10.4239 10.1194 10.2716 10.2716C10.1194 10.4239 9.9391 10.5 9.73077 10.5C9.51442 10.5 9.33414 10.4239 9.1899 10.2716L7.12861 8.21635C6.41146 8.71314 5.61218 8.96154 4.73077 8.96154C4.15785 8.96154 3.60998 8.85036 3.08714 8.62801C2.5643 8.40565 2.11358 8.10517 1.73498 7.72656C1.35637 7.34796 1.05589 6.89724 0.833534 6.3744C0.611177 5.85156 0.5 5.30369 0.5 4.73077C0.5 4.15785 0.611177 3.60998 0.833534 3.08714C1.05589 2.5643 1.35637 2.11358 1.73498 1.73498C2.11358 1.35637 2.5643 1.05589 3.08714 0.833534C3.60998 0.611177 4.15785 0.5 4.73077 0.5C5.30369 0.5 5.85156 0.611177 6.3744 0.833534C6.89724 1.05589 7.34796 1.35637 7.72656 1.73498C8.10517 2.11358 8.40565 2.5643 8.62801 3.08714C8.85036 3.60998 8.96154 4.15785 8.96154 4.73077C8.96154 5.61218 8.71314 6.41146 8.21635 7.12861L10.2776 9.1899C10.4259 9.33814 10.5 9.51843 10.5 9.73077Z"
                    fill="white" />
            </svg></a>
    </div>
    <button class="site-header__nav-icon mobile-nav-icon no-print" id="mobile-nav-icon" aria-expanded="false" aria-controls="mobile-nav">
        <span class="screen-reader-text">{!! _e('Navigation Toggle', 'visceral') !!}</span>
        <div>
            <div class="mobile-nav-icon__line"></div>
            <div class="mobile-nav-icon__line"></div>
            <div class="mobile-nav-icon__line"></div>
            <div class="mobile-nav-icon__line"></div>
        </div>
    </button>
</header>
@include('partials.mobile-menu')
@include('partials.site-search')