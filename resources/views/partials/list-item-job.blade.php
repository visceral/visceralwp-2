@php
$location_city = get_post_meta(get_the_id(), 'location_city', true);
$location_state = get_post_meta(get_the_id(), 'location_state', true);@endphp

<div class="col col-12">
    <article class="list-item-job">
        <div class="list-item-job__main">
            <h1 class="list-item-job__title h4">
                <a href="{{ get_permalink($ID)}}">{{ get_the_title($ID) }}</a>
            </h1>
        </div>
        <div class="list-item-job__secondary">
            @if ($location_city && $location_state)
            <div class="list-item-job__location">
                <p>{{ $location_city }}, {{ $location_state }}</p>
            </div>
            @endif
            <time class="list-item-job__date">
                Posted {{ get_the_date('F j, Y', $ID) }}
            </time>
        </div>
    </article>
</div>