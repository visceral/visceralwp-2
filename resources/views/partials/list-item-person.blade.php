@php
$image = get_the_post_thumbnail($ID, 'medium', array('class' => 'img-crop'));
$job_title = get_post_meta($ID, 'job_title', true);
$organization = get_post_meta($ID, 'organization', true);
@endphp

<div class="col col-12 col-sm-6 col-md-3 {{(!is_null($col_class)) ? $col_class : ''}}">
    <article class="list-item-person">
        @if ($image)
        <a href="{{ get_permalink($ID)}}" class="list-item-person__image">    
            {!! $image !!}
        </a>
        @endif
        <div class="list-item-person__main text-center">
            <h1 class="list-item-person__title">
                <a href="{{ get_permalink($ID)}}">{{ get_the_title($ID) }}</a>
            </h1>
            @if ($job_title)
            <div class="list-item-person__job-title">
                <p>{{ $job_title }}</p>
            </div>
            @endif
            @if ($organization)
            <div class="list-item-person__organization">
                <p class="small">{{ $organization }}</p>
            </div>
            @endif
        </div>
    </article>
</div>