<section class="related-content text-center p-t-64 p-b-24 md-p-b-120 no-print">
    <div class="container">
        <h2 class="h5 related-content__heading">
            {{ $related_heading ? $related_heading: __('Related', 'visceral')}}</h2>
        <div class="row">
            @foreach ($related_content as $related_item)
            @php
            $ID = $related_item->ID;
            @endphp
            @include('partials.list-item-card')
            @endforeach
        </div>
    </div>
</section>