<article {{ post_class('resource-post') }}>
    <header>
        <h1 class="screen-reader-text">{!! App::title() !!}</h1>
    </header>
    <div class="container">
        <div class="row justify-center">
            @if( !empty($thumbnail) )
            <div class="col col-xs-6 col-sm-4 col-md-2 m-b-24">
                <img class="resource-post__image" src="{{ $thumbnail['url'] }}" alt="">
            </div>
            <div class="col col-md-1"></div>
            <div class="col col-sm-8 col-md-6">
                @else
                <div class="col col-md-1"></div>
                <div class="col col-md-8">
                    @endif
                    <div class="resource-post__content post-content">
                        @php the_content() @endphp
                    </div>
                </div>

                <div class="col col-md-1"></div>
                <div class="col col-md-2 resource-post__meta">
                    <div class="">
                        @if($fields['publication_date'])
                        <p class="small resource-post__meta-label">Published</p>
                        <p class="small">{!! $fields['publication_date'] !!}</p>
                        @endif

                        @if($type_links)
                        <p class="small resource-post__meta-label">Type</p>
                        <p class="small">{!! $type_links !!}</p>
                        @endif

                        @if($topic_links)
                        <p class="small resource-post__meta-label">Topic</p>
                        <p class="small">{!! $topic_links !!}</p>
                        @endif

                        @if($fields['publication_author'])
                        <p class="small resource-post__meta-label">Author</p>
                        <p class="small">{!! $fields['publication_author'] !!}</p>
                        @endif

                        <p class="small">Share</p>
                    </div>
                </div>
            </div>
        </div>

        @if( $related_content )
        @include('partials.related-content')
        @endif
</article>

<?php
	/**
	 * Adding JSON-LD for structured data and SEO
	 *
	 * Official website: 	http://json-ld.org/
	 * Examples: 			http://jsonld.com/
	 * Generator: 			https://www.schemaapp.com/tools/jsonld-schema-generator/
	 * Testing: 			https://search.google.com/structured-data/testing-tool/u/0/
	 * Google Docs: 		https://developers.google.com/search/docs/data-types/data-type-selector
	 **/

	$logo_url = get_site_icon_url(); // Feel free to customize. Otherwise will use WordPress site icon

	if ( $logo_url && has_post_thumbnail() ) :
		// Images are required. We only continue if we have them.
		$featured_img = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', true );
		
		// JSON-LD markup for Articles
		$json_ld = array(

			// Required
			"@context" => "http://schema.org",
			"@type" => "Article",
			"publisher" => array(	 
				"@type" => "Organization",
				"url" => get_site_url(),
				"name" => get_bloginfo('name'),
				"logo" => array(
							"@type" => "ImageObject",
				            "name" => get_bloginfo('name') . " Logo",
				            "width" => "512",
				            "height" => "512",
				            "url" => $logo_url
				),
			),
			"headline" => get_the_title(),
			"author" => array(
				"@type" => "Person",
				"name" => get_the_author()
			),
			"datePublished" => get_the_date(),
			"image" => array(
				"@type" => "ImageObject",
				"url" => $featured_img[0],
				"width" => $featured_img[1],
				"height" => $featured_img[2]
			),

			// Recommended
			"dateModified" =>  get_the_modified_date(),
			"mainEntityOfPage" => get_permalink(),

			// Optional (Not sure if they affect SEO)
			// "url" => get_permalink(),
			// "description" => get_the_excerpt(),
			// "articleBody" => get_the_content(),
		);
		?>
<script type='application/ld+json'>
    <?php echo json_encode( $json_ld ); ?>
</script>
<?php endif; ?>