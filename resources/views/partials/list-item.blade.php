@php
$image = get_the_post_thumbnail($ID, 'large', array('class' => 'img-crop img-crop--3-2'));
$types = App\visc_get_the_term_list($ID, 'news_type', ', ');
$icon = get_post_meta($ID, 'icon', true);
@endphp

<div class="col col-12">
    <article class="list-item {{$image ? 'list-item--featured-image' : ''}}">
        <div class="row">
            @if ($image)
            <div class="col col-12 col-md-4">
                <a href="{{ get_permalink($ID)}}" class="list-item__image">
                    {!! $image !!}     
                </a>
            </div>
            @endif
            <div class="col col-12 col-md list-item-column">
                <div class="list-item__main">
                    @if ($types)
                    <div class="list-item__types post-label">
                        <p>{{ $types }}</p>
                    </div>
                    @endif
                    <h1 class="list-item__title">
                        <a href="{{ get_permalink($ID)}}">{{ get_the_title($ID) }}</a>
                    </h1>
                    @if( get_post_type() != 'resource' )
                    <time class="list-item__date">
                        {{ get_the_date('F j, Y', $ID) }}
                    </time>
                    @endif
                    @if ($icon === 'Video')
                    <svg class="list-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M12 0C15.3052 0 18.1314 1.1737 20.4789 3.52113C22.8263 5.86856 24 8.69482 24 12C24 15.3052 22.8263 18.1314 20.4789 20.4789C18.1314 22.8263 15.3052 24 12 24C8.69482 24 5.86856 22.8263 3.52113 20.4789C1.1737 18.1314 0 15.3052 0 12C0 8.69482 1.1737 5.86856 3.52113 3.52113C5.86856 1.1737 8.69482 0 12 0Z"
                            fill="#062DB3" />
                        <path d="M15.3334 12L10 16V8L15.3334 12Z" fill="white" />
                    </svg>
                    @elseif ($icon === 'External Link')
                    <svg class="list-item__icon" width="24" height="24" viewBox="0 0 24 24" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M12 0C15.3052 0 18.1314 1.1737 20.4789 3.52113C22.8263 5.86856 24 8.69482 24 12C24 15.3052 22.8263 18.1314 20.4789 20.4789C18.1314 22.8263 15.3052 24 12 24C8.69482 24 5.86856 22.8263 3.52113 20.4789C1.1737 18.1314 0 15.3052 0 12C0 8.69482 1.1737 5.86856 3.52113 3.52113C5.86856 1.1737 8.69482 0 12 0Z"
                            fill="#062DB3" />
                        <path
                            d="M14.6667 14.6667H9.33333V9.35417L10.6667 9.33333V8H8V16H16V12.6667H14.6667V14.6667ZM12 8L13.3333 9.33333L11.3333 11.3333L12.6667 12.6667L14.6667 10.6667L16 12V8H12Z"
                            fill="white" />
                    </svg>
                    @elseif ($icon === 'Download')
                    <svg class="list-item__icon" width="25" height="24" viewBox="0 0 25 24" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M12.6015 0C15.8515 0 18.6305 1.1737 20.9387 3.52113C23.247 5.86856 24.401 8.69482 24.401 12C24.401 15.3052 23.247 18.1314 20.9387 20.4789C18.6305 22.8263 15.8515 24 12.6015 24C9.35156 24 6.57252 22.8263 4.2643 20.4789C1.95609 18.1314 0.802002 15.3052 0.802002 12C0.802002 8.69482 1.95609 5.86856 4.2643 3.52113C6.57252 1.1737 9.35156 0 12.6015 0Z"
                            fill="#062DB3" />
                        <path
                            d="M8.66821 15.5906H16.5346V16.745H8.66821V15.5906ZM16.5346 10.4363L12.6014 14.4363L8.66821 10.4363H10.912V7H14.2908V10.4363H16.5346Z"
                            fill="white" />
                    </svg>
                    @endif
                </div>
            </div>
        </div>
    </article>
</div>