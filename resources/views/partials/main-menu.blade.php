<nav class="site-header__navigation">
    @if (has_nav_menu('primary_menu'))
    {!! App\visc_bem_menu('primary_menu') !!}
    @endif
</nav>