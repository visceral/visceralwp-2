@php
/**
* Masthead
* Search results
*
*/
@endphp

<div class="masthead masthead--search">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="masthead__content">
                    <div class="search-results-top">
                        <form action="{{ site_url() }}" class="row">
                            <div class="search-results-input col col-sm-3">
                                <label><span class="screen-reader-text">{{ __('Search', 'visceral')}}</span>
                                    <input type="text" name="s" value="{{ $original_search }}" class="js-show">
                                </label>
                            </div>
                            <div class="search-results-filter col col-sm-2">
                                <label><span class="screen-reader-text">{{ __('Filter By:', 'visceral')}}</span>
                                    <select name="post_type">{{!! $filter_items !!}}</select>
                                </label>
                            </div>
                            <div class="search-results-submit js-show col col-sm-2">
                                <input type="submit" value="{{ __('Submit', 'visceral')}}">
                            </div>
                        </form>
                    </div>
                    <h1 class="masthead__title h5">{{ $total }} {!! App::title() !!}</h1>
                </div>
            </div>
        </div>
    </div>
</div>