<article @php post_class('single-job__main') @endphp>
    <header>
        <h1 class="screen-reader-text">{!! App::title() !!}</h1>
    </header>
    <div class="single-job__content post-content">
        <div class="container">
            <div class="row justify-center">
                <div class="col col-12 col-md-10">
                    @php the_content() @endphp
                </div>
            </div>
        </div>
    </div>
</article>
<?php
	/**
	 * Adding JSON-LD for structured data and SEO
	 *
	 * Official website: 	http://json-ld.org/
	 * Examples: 			http://jsonld.com/
	 * Generator: 			https://www.schemaapp.com/tools/jsonld-schema-generator/
	 * Testing: 			https://search.google.com/structured-data/testing-tool/u/0/
	 * Google Docs: 		https://developers.google.com/search/docs/data-types/data-type-selector
	 **/

	$logo_url = get_site_icon_url(); // Feel free to customize. Otherwise will use WordPress site icon
    $location_city = get_post_meta(get_the_id(), 'location_city', true);
    $location_state = get_post_meta(get_the_id(), 'location_state', true);

	if ($location_city && $location_state) :
		// Images are required. We only continue if we have them.
		$featured_img = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', true );
		
		// JSON-LD markup for Articles
		$json_ld = array(

			// Required
			"@context" => "http://schema.org",
			"@type" => "JobPosting",
			"hiringOrganization" => array(	 
				"@type" => "Organization",
				"url" => get_site_url(),
				"name" => get_bloginfo('name'),
				
			),
			"title" => get_the_title(),
            "description" => get_the_content(),
            "jobLocation" => array(
                "@type" => "Place",
                "address" => array(
                    "@type" => "PostalAddress",
                    "addressLocality" => $location_city,
                    "addressRegion" => $location_state,
                    "addressCountry" => "US"
                )
            ),
			"datePosted" => get_the_date(),

			// Recommended
			"mainEntityOfPage" => get_permalink(),

			// Optional (Not sure if they affect SEO)
			// "url" => get_permalink(),
			// "description" => get_the_excerpt(),
			// "articleBody" => get_the_content(),
		);
		?>
<script type='application/ld+json'>
    <?php echo json_encode( $json_ld ); ?>
</script>
<?php endif; ?>