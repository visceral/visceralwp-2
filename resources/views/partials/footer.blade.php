<footer class="site-footer">
    <div class="container">
        <div class="row text-white justify-space-between">
            <div class="col col-12 col-md-6 site-footer__left">
                <a class="site-footer__logo" href="/">
                    <span class="screen-reader-text">{{ __('Home', 'visceral')}}</span>
                    @include('graphics.footer-logo')
                </a>
                @php dynamic_sidebar('sidebar-footer') @endphp
                @if (has_nav_menu('footer_menu'))
                {!! App\visc_bem_menu('footer_menu', 'footer-menu') !!}
                @endif
            </div>
            <div class="col col-12 col-md-6 site-footer__right">
                @if( $social_links )
                    {!! $social_links !!}
                @endif
                <p class="site-footer__copyright">
                    &copy; {{date('Y')}} {{ get_theme_mod( 'copyright_textbox', get_bloginfo('name') )}}
                    @if( get_page_by_path('privacy-policy') )
                    &emsp; <a href="{{ get_permalink( get_page_by_path('privacy-policy') ) }}">Legal</a>
                    @endif
                </p>
            </div>
        </div>
    </div>
</footer>