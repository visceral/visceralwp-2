<div class="site-search">
    <div class="container">
        <div class="site-search__inner text-white">
            <form role="search" class="site-search__form searchform" method="get" action="{{ site_url() }}">
                <label><span class="screen-reader-text">{{ __('Search', 'visceral') }}</span>
                    <input type="text" placeholder="{{ __('Search everything', 'visceral') }}" name="s"
                        autocomplete="off" spellcheck="false">
                </label>
                <input type="submit" class="screen-reader-text" value="{{ __('Submit', 'visceral') }}">
            </form>
            <span class="screen-reader-text">{{ __('Close', 'visceral') }}</span>
            <button class="site-search__close icon-cancel">
                <svg aria-label="Close" width="32" height="32" viewBox="0 0 32 32" fill="none"
                    xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M32 3.125L19.2215 15.5208L32 27.9167L28.7785 31.0417L16 18.6458L3.22148 31.0417L0 27.9167L12.7785 15.5208L0 3.125L3.22148 0L16 12.3958L28.7785 0L32 3.125Z"
                        fill="white" />
                </svg>
            </button>

        </div>
    </div>
</div>