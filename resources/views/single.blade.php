@extends('layouts.app')
@if (!is_singular('person'))
@section('masthead')
@include('partials.masthead-'.get_post_type())
@endsection
@endif


@section('content')
@while(have_posts()) @php the_post() @endphp
@include('partials.content-single-'.get_post_type())
@endwhile
@endsection